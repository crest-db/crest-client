from setuptools import setup, find_packages

NAME = "pycrest-client"
VERSION = "5.0.0"
REQUIRES = [
  "urllib3 >= 1.25.3",
  "python-dateutil",
  "hep.crest.client",
  "requests",
  "pysocks"
]

setup(
    name=NAME,
    version=VERSION,
    description="Client for CREST Server on Python",
    author="",
    author_email="",
    url="",
    keywords=["CREST", "CREST Server"],
    python_requires=">=3.6",
    install_requires=REQUIRES,
    packages=find_packages(where='.', exclude=["test", "tests"]),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'pycrest = pycrest.cli.pycrest_cli:main',
        ],
    },
    scripts=['scripts/crest-python-setup.sh', 'scripts/crest-server-path-setup.sh'],
    license="Apache 2.0",
    long_description="""\
    Client API for CREST Server on Python
    """
)
