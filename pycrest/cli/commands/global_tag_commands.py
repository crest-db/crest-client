
from pycrest.api.crest_api import CrestApi
from pycrest.cli.commands.utils import print_full_res, print_multiple_res
from hep.crest.client.models import (
    IovSetDto, HTTPResponse, TagMetaSetDto, TagMetaDto, TagSetDto, TagDto, GlobalTagDto, 
    GlobalTagSetDto, GlobalTagMapDto, StoreSetDto, StoreDto, RunLumiInfoDto, RunLumiSetDto
)
import sys

def create_global_tag_func(args):

    required_args = ['description']
    missing_args = [arg for arg in required_args if getattr(args, arg) is None]
    if missing_args:
        sys.exit(f"Error: The following parameters are not set: {', '.join(missing_args)}")

    params = {
        'name': args.name,
        'validity': args.validity,
        'description': args.description,
        'release': args.release,
        'scenario': args.scenario,
        'workflow': args.workflow,
        'type': args.type,
        'force': 'false'
    }

    globaltag = GlobalTagDto(
        name=params['name'],
        description=params['description'],
        release=params['release'],
        scenario=params['scenario'],
        validity=params['validity'],
        workflow=params['workflow'],
        type=params['type']
    )
    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.create_global_tag(dto=globaltag, force=params['force'])
        # print(f"global tag {args.name} created")
        print_full_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))


def get_global_tag_list_func(args):
    # print("GET GLOBAL TAG LIST function")
    # print(f"command = {args.command}")
    # print(f"name = {args.name}")
    # print(f"host = {crest_host}")
    name = args.name
    # optional arguments
    optional_args = ['page', 'size']
    arg_dict = vars(args)
    params = {}
    for arg in args.__dict__.keys():
        if arg in optional_args:
            params[arg] = arg_dict[arg]
    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.list_global_tags(name=name, **params)
        # print_json(resp)
        print_multiple_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))


def remove_global_tag_func(args):
    # print("REMOVE GLOBAL TAG function")
    # print(f"command = {args.command}")
    # print(f"name = {args.name}")
    # print(f"host = {args.crest_host}")
    command = args.command
    name = args.name
    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.remove_global_tag(name=name)
        # print(f"tag {name} deleted")
        print(f'Removed global tag {name} : {resp}')
    except Exception as e:
        print("Error: "+ repr(e))



def get_global_tag_map_func(args):
    # print("GET GLOBAL TAG MAP function")
    # print(f"command = {args.command}")
    # print(f"name = {args.name}")
    # print(f"host = {crest_host}")
    name = args.name
    # optional arguments
    optional_args = ['mode']
    arg_dict = vars(args)
    params = {}
    for arg in args.__dict__.keys():
        if arg in optional_args:
            params[arg] = arg_dict[arg]
    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.find_global_tag_map(name=name, **params)
        print_multiple_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))


def create_global_tag_map_func(args):
    required_args = ['record', 'label', 'tag_name', 'name']
    missing_args = [arg for arg in required_args if getattr(args, arg) is None]
    if missing_args:
        sys.exit(f"Error: The following parameters are not set: {', '.join(missing_args)}")

    params = {
        'record': args.record,
        'label': args.label,
    }

    globaltagmap = GlobalTagMapDto(
        global_tag_name=args.name,
        tag_name=args.tag_name,
        record=params['record'],
        label=params['label']
    )

    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.create_global_tag_map(globaltagmap)
        # print(f"global tag map {args.name} - {args.tag_name} created")
        print_full_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))

def remove_global_tag_map_func(args):
    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.delete_global_tag_map(globaltagname=args.name, tagname=args.tag_name, label=args.label)
        print(f'Unlink {args.tag_name} from {args.name}')
    except Exception as e:
        print("Error: "+ repr(e))

def clone_global_tag_map_func(args):
    """
    Clone a global tag map to a new global tag
    """
    name = args.name
    destname = args.dest_globaltag
    excludes = args.excludes
    excludes_list = excludes.split(',')
    try:
        crest_api = CrestApi(host=args.crest_host)
        # Retrieve the global tag map for source global tag
        resp = crest_api.find_global_tag_map(name=name)
        print(f"Found mappings for source: {resp}")
        map_list = resp.resources
        # Loop over the tag map and create the new tag map filtering out the excludes
        for tagmap in map_list:
            print(f"tagmap: {tagmap.tag_name} - {tagmap.record} - {tagmap.label}")
            if tagmap.tag_name not in excludes_list:
                print(f"Cloning tag {tagmap.tag_name} to {destname}")
                globaltagmap = GlobalTagMapDto(
                    global_tag_name=destname,
                    tag_name=tagmap.tag_name,
                    record=tagmap.record,
                    label=tagmap.label
                )
                resp = crest_api.create_global_tag_map(globaltagmap)
                print_full_res(resp)
            else:
                print(f"Excluded tag {tagmap.tag_name} from cloning")
    except Exception as e:
        print("Error: "+ repr(e))