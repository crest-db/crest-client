from pycrest.cli.commands.utils import print_json, print_multiple_res
from pycrest.api.crest_api import CrestApi
import sys

def get_iov_list_func(args):
    # print("GET IOV LIST function")
    # print(f"command = {args.command}")
    # print(f"name = {args.name}")
    # print(f"since = {args.since}")
    # print(f"until = {args.until}")
    # print(f"host = {crest_host}")
    # tag name is mandatory
    name = args.name
    # optional arguments
    optional_args = ['since', 'until', 'method', 'snapshot', 'timeformat', 'page', 'size', 'groupsize']
    arg_dict = vars(args)
    params = {}
    for arg in args.__dict__.keys():
        if arg in optional_args:
            if ((arg=='until') and (args.until=='-1')):
                params[arg] = 'INF'
            elif (arg_dict[arg]!=None):
                params[arg] =arg_dict[arg]

    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.find_all_iovs(tagname=name, **params)

        print_multiple_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))

