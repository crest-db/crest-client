from pycrest.api.crest_api import CrestApi
from pycrest.cli.commands.utils import print_multiple_res, print_full_res, print_single_res
from pycrest.cli.config import crest_host
from hep.crest.client.models import (
    IovSetDto, HTTPResponse, TagMetaSetDto, TagMetaDto, TagSetDto, TagDto, GlobalTagDto, IovDto,
    GlobalTagSetDto, GlobalTagMapDto, StoreSetDto, StoreDto, RunLumiInfoDto, RunLumiSetDto
)
import sys

def get_tag_func(args):
    # print("GET TAG function")
    # print(f"command = {args.command}")
    # print(f"name = {args.name}")
    name = args.name
    # optional arguments
    optional_args = ['page', 'size']
    arg_dict = vars(args)
    params = {}
    for arg in args.__dict__.keys():
        if arg in optional_args:
            params[arg] = arg_dict[arg]
    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.list_tags(name=name, **params)
        print_multiple_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))



def create_tag_func(args):
    # print("CREATE TAG function")
    required_args = ['time_type', 'payload_spec', 'description', 'synchronization', 'last_validated_time', 'end_of_validity']
    missing_args = [arg for arg in required_args if getattr(args, arg) is None]
    if missing_args:
        sys.exit(f"Error: The following parameters are not set: {', '.join(missing_args)}")

    params = {
        'payload_spec': args.payload_spec,
        'description': args.description,
        'synchronization': args.synchronization,
        'last_validated_time': int(args.last_validated_time),
        'end_of_validity': int(args.end_of_validity),
    }
    tag = TagDto(
        name=args.name,
        description=params['description'],
        time_type=args.time_type,
        payload_spec=params['payload_spec'],
        synchronization=params['synchronization'],
        last_validated_time=params['last_validated_time'],
        end_of_validity=params['end_of_validity']
    )
    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.create_tag(dto=tag)
        # print(f"tag {args.name} created")
        print_full_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))


def remove_tag_func(args):
    # print("GET TAG function")
    # print(f"command = {args.command}")
    # print(f"name = {args.name}")
    # print(f"host = {args.crest_host}")
    name = args.name

    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.remove_tag(name=name)
        print(f'Removed tag {name} : {resp}')
    except Exception as e:
        print("Error: "+ repr(e))

def create_tag_meta_info_func(args):
    """
    :param args: the parameters to create tag meta information.
    """

    required_args = ['description', 'chansize', 'colsize', 'tag_info']
    missing_args = [arg for arg in required_args if getattr(args, arg) is None]
    if missing_args:
        sys.exit(f"Error: The following parameters are not set: {', '.join(missing_args)}")

    # Open the tag_info file and read the content
    tag_info = None
    with open(args.tag_info, 'r') as f:
        tag_info = f.read()

    params = {
        'description': args.description,
        'chansize': args.chansize,
        'colsize': args.colsize,
        'tag_info': tag_info,
    }

    tag_meta = TagMetaDto(
        tag_name=args.name,
        description=params['description'],
        chansize=params['chansize'],
        colsize=params['colsize'],
        tag_info=params['tag_info'],
    )

    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.create_tag_meta(dto=tag_meta)
        # print(f"tag meta info {args.name} created")
        print_full_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))


def get_tag_meta_info_func(args):
    # print("GET TAG META INFO function")
    # print(f"command = {args.command}")
    # print(f"name = {args.name}")
    # print(f"host = {crest_host}")
    command = args.command
    name = args.name

    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.find_tag_meta(name=name)
        print_single_res(resp)
    except Exception as e:
        print("Error: "+ repr(e))


def get_tag_size_func(args):
    # print("GET TAG SIZE function")
    # print(f"command = {args.command}")
    # print(f"name = {args.name}")
    # print(f"host = {crest_host}")
    command = args.command
    name = args.name

    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.get_size(tagname=name)
        # print_json(resp)
        print(f"tag size ({name}) = {resp} IOVs")
    except Exception as e:
        print("Error: "+ repr(e))

def clone_tag_func(args):
    """
    Clone a global tag map to a new global tag
    """
    name = args.name
    destname = args.dest_tag
    try:
        crest_api = CrestApi(host=args.crest_host)
        # Retrieve the tag for source tag
        resp = crest_api.find_tag(name=name)
        source_tag = resp
        print(f"Found tag for source: {source_tag}")
        dest_tag = TagDto(
            name=destname,
            description=source_tag.description,
            time_type=source_tag.time_type,
            payload_spec=source_tag.payload_spec,
            synchronization=source_tag.synchronization,
            last_validated_time=source_tag.last_validated_time,
            end_of_validity=source_tag.end_of_validity
        )
        # Create the new tag
        resp = crest_api.create_tag(dto=dest_tag)

        # Retrieve the tag meta info for the source tag
        resp = crest_api.find_tag_meta(name=name)
        source_tag_meta = resp['resources'][0]
        print(f"Found tag meta for source: {source_tag_meta}")
        dest_tag_meta = TagMetaDto(
            tag_name=destname,
            description=source_tag_meta.description,
            chansize=source_tag_meta.chansize,
            colsize=source_tag_meta.colsize,
            tag_info=source_tag_meta.tag_info
        )
        # Create the new tag meta info
        resp = crest_api.create_tag_meta(dto=dest_tag_meta)
        #
        print(f"Copy iovs from tag {name} to {destname}")
        # Load iovs from source tag
        iovsetdto = crest_api.find_all_iovs(tagname=name)
        if iovsetdto is None:
            print(f"No iovs found for tag {name}")
            return
        if isinstance(iovsetdto, HTTPResponse):
            print(f"Error: {iovsetdto}")
            return
        # resp is IovSetDto
        iovs = iovsetdto.resources
        destiovs = []
        previov = None
        for iov in iovs:
            newdto = IovDto(
                tag_name = destname,
                since = iov.since,
                payload_hash = iov.payload_hash
            )
            print(f"Create iovdto: {newdto}")
            if previov is not None and previov.since == iov.since:
                # Check insertion time
                if previov.insertion_time > iov.insertion_time:
                    print(f"Skip iov since {iov.since} with insertion time {iov.insertion_time}")
                    continue
                else:
                    # Remove the last iov from array and store this one
                    print(f"Replace previou iov since {previov.since} with insertion time {previov.insertion_time}")
                    destiovs.pop()
            destiovs.append(newdto)
            previov = iov
        # Create iovs for the new tag
        iovsetdto.resources = destiovs
        resp = crest_api.store_iovs(iovsetdto)

        print(f"Cloned tag {name} to {destname}: iov list of size {len(destiovs)}")
    except Exception as e:
        print("Error: "+ repr(e))