from pycrest.api.crest_api import CrestApi
import json
from hep.crest.client.models import (
    IovSetDto, HTTPResponse, TagMetaSetDto, TagMetaDto, TagSetDto, TagDto, GlobalTagDto, 
    GlobalTagSetDto, GlobalTagMapDto, StoreSetDto, StoreDto, RunLumiInfoDto, RunLumiSetDto
)
from pycrest.cli.config import crest_host

def get_payload_func(args):
    command = args.command
    hash = args.hash
    file = args.file

    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.get_payload(hash=hash)
        with open(file, 'wb') as f:
            f.write(resp)

        print(f"payload {hash} stored in {file}")
    except Exception as e:
        print("Error: "+ repr(e))



def store_payload_func(args):
    name = args.name
    file = args.file
    since = args.since

    storeset = None

    if args.type == 'storeset':
        with open(file, 'r') as f:
            data = f.read()
        dto = json.loads(data)
        res = dto.get('resources')
        resources = []
        for r in res:
            r['streamerInfo'] = '{\"filename\": \"' + file + '\"}'
            storedto = StoreDto(
                since=int(r.get('since')),
                data=r.get('data'),
                streamerInfo=r['streamerInfo']
            )
            resources.append(storedto)
        storeset = StoreSetDto(
            size=dto.get('size'),
            datatype="iovs",
            format="StoreSetDto",
            resources=resources
        )

    else:
        resources = []
        data = None
        with open(file, 'r') as f:
            data = f.read()
        dto = StoreDto(
            since=int(since),
            data=data,
            streamerInfo='{\"filename\": \"' + file + '\"}'
        )
        resources.append(dto)
        storeset = StoreSetDto(
            size=len(resources),
            datatype="iovs",
            format="StoreSetDto",
            resources=resources
        )

    try:
        crest_api = CrestApi(host=args.crest_host)
        resp = crest_api.store_data(tag=name, store_set=storeset)
        print(f"payload for tag {name} stored in CREST from file {file}")
    except Exception as e:
        print("Error: "+ repr(e))

