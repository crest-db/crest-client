#========================================
import json
from datetime import datetime

# Custom serializer for datetime objects
def datetime_serializer(obj):
    if isinstance(obj, datetime):
        return obj.isoformat()

def print_json(obj):
    json_res = json.dumps(obj.to_dict(), default=datetime_serializer)
    print(f'###CREST: {json_res}')


#----------------------------

def print_single_res(data):
    str_res = json.dumps(data.to_dict(), default=datetime_serializer)
    json_res = json.loads(str_res)

    if 'resources' not in data:
        # print(jsonData)
        print_json(data)
    else:
        res = json_res['resources']
        print(json.dumps(res[0], indent=4))


def print_multiple_res(data):
    str_res = json.dumps(data.to_dict(), default=datetime_serializer)
    json_res = json.loads(str_res)

    if 'resources' not in data:
        # print(jsonData)
        print_json(data)
    else:
        res = json_res['resources']
        print(json.dumps(res, indent=4))

def print_full_res(data):
    str_res = json.dumps(data.to_dict(), default=datetime_serializer)
    json_res = json.loads(str_res)

    print(json.dumps(json_res, indent=4))
