import argparse
import sys
import json

from pycrest.cli.commands.tag_commands import get_tag_func, create_tag_func, remove_tag_func
from pycrest.cli.commands.tag_commands import create_tag_meta_info_func, get_tag_meta_info_func, clone_tag_func
from pycrest.cli.commands.global_tag_commands import create_global_tag_func, get_global_tag_list_func, remove_global_tag_func
from pycrest.cli.commands.global_tag_commands import create_global_tag_map_func, get_global_tag_map_func, remove_global_tag_map_func, clone_global_tag_map_func
from pycrest.cli.commands.iovs_commands import get_iov_list_func
from pycrest.cli.commands.tag_commands import get_tag_size_func
from pycrest.cli.commands.payload_commands import get_payload_func, store_payload_func
from pycrest.cli.config import crest_host
from pycrest.cli.config import crest_path_param

import requests

def get_crest_path():
    import os
    try:
        path = os.environ[crest_path_param]  
        return path
    except KeyError:
        return ''

def socks(url: str = 'localhost:3129') -> None:
    SOCKS5_PROXY_HOST = url.split(':')[0]
    SOCKS5_PROXY_PORT = int(url.split(':')[1])
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

def print_commands():
    cli_commands = """

    Python CREST client commands: 

    pycrest get tag [OPTIONS]
    pycrest create tag [OPTIONS]
    pycrest get iovList [OPTIONS]
    pycrest get tagSize [OPTIONS]
    pycrest clone tag [OPTIONS]

    pycrest create globalTag [OPTIONS]
    pycrest create globalTagMap [OPTIONS]
    pycrest get globalTag [OPTIONS]
    pycrest get globalTagMap [OPTIONS]
    pycrest clone globalTagMap [OPTIONS]

    pycrest remove tag [OPTIONS]
    pycrest remove globalTag [OPTIONS]
    pycrest remove globalTagMap [OPTIONS]

    pycrest create tagMetaInfo [OPTIONS]
    pycrest get tagMetaInfo [OPTIONS]

    pycrest get payload [OPTIONS]
    pycrest store payload [OPTIONS]

    pycrest get commands
    pycrest get version
    """
    print (cli_commands)



# ==========================================
def get_version(crest_host):

    from urllib.parse import urlparse
    # from urlparse import urlparse  # Python 2

    url = crest_host
    parsed_uri = urlparse(url)
    result = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

    # host = result + "actuator/info"
    host = result + "mgmt/info"

    resp = requests.get(host)

    # If the HTTP GET request can be served
    if resp.status_code == 200:
        data = json.loads(resp.content)

        if 'build' not in data:
            raise ValueError(f"No target \"build\" in the string {data}")
        else:
            build = data["build"]
            if 'version' not in build:
                raise ValueError(f"No target \"version\" in the string {build}")
            else:
                vers = build["version"]
                print(f"CREST server API version = {vers}")

#===================================================================
def call_function(parser: argparse.ArgumentParser, args, unknown_args):
    parsed_args = parser.parse_args(unknown_args)
    parsed_args_dict = vars(parsed_args)
    args.__dict__.update(parsed_args_dict)

    # Call the function with updated arguments
    if not args.__dict__:
        parser.print_usage()
    else:
        args.func(args)


#=======================================================

def get_iov_list_command(args, unknown_args):
    # print("GET IOV LIST command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.add_argument('--since', dest = 'since',required = False, help = 'Since time')
    parser.add_argument('--until', dest = 'until',required = False, help = 'Until time')
    parser.add_argument('--method', dest = 'method', default='IOVS', required = False, help = 'Method used [IOVS, GROUPS, MONITOR]')
    parser.add_argument('--timeformat', dest = 'timeformat', default='NUMBER', required = False, help = 'Timeformat [NUMBER, ISO, ...]')
    parser.add_argument('--page', dest='page', default=0, type=int, required=False, help='Select page number')
    parser.add_argument('--size', dest='size', default=1000, type=int, required=False, help='Number of entries in a page')
    parser.set_defaults(func=get_iov_list_func)

    call_function(parser, args, unknown_args)

def create_tag_command(args, unknown_args):
    # print("CREATE TAG command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.add_argument('-time_type',           dest = 'time_type',          required = False, default='time', help = 'time type')
    parser.add_argument('-description',         dest = 'description',        required = False, default='test', help = 'description')
    parser.add_argument('-payload_spec',        dest = 'payload_spec',       required = False, default='JSON', help = 'payload specification')
    parser.add_argument('-synchronization',     dest = 'synchronization',    required = False, default='all',  help = 'synchronization')
    parser.add_argument('-last_validated_time', dest = 'last_validated_time',required = False, default=0.0,    help = 'last validated time', type = float)
    parser.add_argument('-end_of_validity',     dest = 'end_of_validity',    required = False, default=-1.0,   help = 'end of validity',     type = float)
    parser.set_defaults(func=create_tag_func)

    call_function(parser, args, unknown_args)


def get_tag_command(args, unknown_args):
    # print("GET TAG command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.add_argument('--page', dest='page', default=0, type=int, required=False, help='Select page number')
    parser.add_argument('--size', dest='size', default=1000, type=int, required=False, help='Number of entries in a page')
    parser.set_defaults(func=get_tag_func)

    call_function(parser, args, unknown_args)


def remove_tag_command(args, unknown_args):
    # print("REMOVE TAG command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.set_defaults(func=remove_tag_func)

    call_function(parser, args, unknown_args)


def create_tag_meta_info_command(args, unknown_args):
    # print("CREATE TAG META INFO command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.add_argument('-description', dest = 'description',required = False, default='test',     help = 'description')
    parser.add_argument('-chansize',    dest = 'chansize',   required = False, default=1,          help = 'number of channels', type = int)
    parser.add_argument('-colsize',     dest = 'colsize',    required = False, default=1,          help = 'number of columns',  type = int)
    parser.add_argument('-tag_info',    dest = 'tag_info',   required = False, default='tag info', help = 'tag info file name')

    parser.set_defaults(func=create_tag_meta_info_func)
    call_function(parser, args, unknown_args)


def get_tag_meta_info_command(args, unknown_args):
    # print("GET TAG META INFO command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.set_defaults(func=get_tag_meta_info_func)

    call_function(parser, args, unknown_args)


def get_payload_command(args, unknown_args):
    # print("GET PAYLOAD command")
    global crest_host
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'hash',required = True, help = 'paylaod hash')
    parser.add_argument('-file',  dest = 'file',required = True,   help = 'file to store payload')
    parser.set_defaults(func=get_payload_func)

    call_function(parser, args, unknown_args)


def store_payload_command(args, unknown_args):
    # print("STORE PAYLOD command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.add_argument('-file',  dest = 'file',required = True,   help = 'file to store payload')
    parser.add_argument('-since', dest = 'since',required = True, help = 'since time', type = int) # MvG
    parser.add_argument('-fmt', dest = 'type',required = False, default='list', help = 'Type of input file: [storeset | list]')
    parser.set_defaults(func=store_payload_func)
    call_function(parser, args, unknown_args)


def create_global_tag_command(args, unknown_args):
    # print("CREATE GLOBAL TAG command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n',           dest = 'name',required = True, help = 'global tag name')
    parser.add_argument('-validity',    dest = 'validity',   required = False, default=0, help = 'validity', type = int)
    parser.add_argument('-description', dest = 'description',required = False, default='test', help = 'description')
    parser.add_argument('-release',     dest = 'release',    required = False, default='1.0', help = 'release')
    parser.add_argument('-scenario',    dest = 'scenario',   required = False, default='none', help = 'scenario')
    parser.add_argument('-workflow',    dest = 'workflow',   required = False, default='none', help = 'workflow')
    parser.add_argument('-type',        dest = 'type',       required = False, default='A', help = 'type')

    parser.set_defaults(func=create_global_tag_func)
    call_function(parser, args, unknown_args)


def get_global_tag_command(args, unknown_args):
    # print("GET GLOBAL TAG command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.add_argument('--page', dest='page', default=0, type=int, required=False, help='Select page number')
    parser.add_argument('--size', dest='size', default=1000, type=int, required=False, help='Number of entries in a page')
    parser.set_defaults(func=get_global_tag_list_func)

    call_function(parser, args, unknown_args)

def remove_global_tag_command(args, unknown_args):
    # print("REMOVE GLOBAL TAG command")
    global crest_host
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'global tag name')
    parser.set_defaults(func=remove_global_tag_func)

    call_function(parser, args, unknown_args)


def get_tag_size_command(args, unknown_args):
    # print("GET TAG SIZE command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'tag name')
    parser.set_defaults(func=get_tag_size_func)

    call_function(parser, args, unknown_args)


def get_global_tag_map_command(args, unknown_args):
    # print("GET GLOBAL TAG MAP command")
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest = 'name',required = True, help = 'global tag name or tag name, depending on the --mode option')
    parser.add_argument('--mode', dest = 'mode', required = False, choices=["Trace","BackTrace"], default = "Trace", help = 'Show mappings between tags and global tags. With mode Trace the name is a global tag name, with mode BackTrace the name is a tag name. Default is Trace.')
    parser.set_defaults(func=get_global_tag_map_func)

    call_function(parser, args, unknown_args)

def create_global_tag_map_command(args, unknown_args):
    # print("CREATE GLOBAL TAG MAP command")
    parser = argparse.ArgumentParser()
    parser.add_argument('--globaltag', dest = 'name',required = True, help = 'global tag name')
    parser.add_argument('--tag', dest = 'tag_name', required = True, help = 'tag name')
    parser.add_argument('--record', dest = 'record',  required = False, help = 'record',  default = 'none')
    parser.add_argument('--label',  dest = 'label',   required = True, help = 'label')
    parser.set_defaults(func=create_global_tag_map_func)

    call_function(parser, args, unknown_args)

def clone_global_tag_map_command(args, unknown_args):
    # print("CLONE GLOBAL TAG MAP command")
    parser = argparse.ArgumentParser()
    parser.add_argument('--globaltag', dest = 'name',required = True, help = 'global tag name')
    parser.add_argument('--destination', dest = 'dest_globaltag', required = True, help = 'destination global tag name (should exists)')
    parser.add_argument('--excludes', dest = 'excludes',  required = False, help = 'Exclude patterns',  default = 'none')
    parser.set_defaults(func=clone_global_tag_map_func)

    call_function(parser, args, unknown_args)

def clone_tag_command(args, unknown_args):
    # print("CLONE TAG command")
    parser = argparse.ArgumentParser()
    parser.add_argument('--tag', dest = 'name',required = True, help = 'tag name')
    parser.add_argument('--destination', dest = 'dest_tag', required = True, help = 'destination tag name')
    parser.set_defaults(func=clone_tag_func)

    call_function(parser, args, unknown_args)

def remove_global_tag_map_command(args, unknown_args):
    # print("REMOVE GLOBAL TAG MAP command")
    parser = argparse.ArgumentParser()
    parser.add_argument('--globaltag', dest = 'name',required = True, help = 'global tag name')
    parser.add_argument('--tag', dest = 'tag_name',required = True, help = 'tag name')
    parser.add_argument('--label', dest = 'label', required = True, help = 'label')
    parser.set_defaults(func=remove_global_tag_map_func)

    call_function(parser, args, unknown_args)

#===================================================================

def main():
    crest_url = get_crest_path()
    if crest_url == '':
        crest_url = 'http://crest-j23.cern.ch:8080/api-v5.0'
        
    parser = argparse.ArgumentParser(description="Python CREST Client", add_help=False)
    parser.add_argument("command", choices=["get", "remove", "create", "store", "clone"], help="Command to execute")
    parser.add_argument("type", choices=["help", "commands", "version", "tag", "iovList", "tagMetaInfo", "payload", "globalTag", "tagSize", "globalTagMap"], help="Type of the command")
    parser.add_argument("--host", dest = 'crest_host', default = crest_url, help="Base URL of the REST API", required = False)
    parser.add_argument("--socks", action='store_true', help="Activate socks")
    print(f'======== Python CREST client =========')
    args, unknown_args = parser.parse_known_args()
    crest_host = args.crest_host
    if args.socks:
        socks()
    if args.command == "get":
        if args.type == "help":
            parser.print_help()
            return
        elif args.type == "commands":
            print_commands()
        elif args.type == "version":
            get_version(crest_host)
        elif args.type == "tag":
            get_tag_command(args, unknown_args)
        elif args.type == "iovList":
            get_iov_list_command(args, unknown_args)
        elif args.type == "tagMetaInfo":
            get_tag_meta_info_command(args, unknown_args)
        elif args.type == "payload":
            get_payload_command(args, unknown_args)
        elif args.type == "globalTag":
            get_global_tag_command(args, unknown_args)
        elif args.type == "tagSize":
            get_tag_size_command(args, unknown_args)
        elif args.type == "globalTagMap":
            get_global_tag_map_command(args, unknown_args)
        else:
            print(f"Unsupported 'get' command type: {args.type}, use 'tag', 'iovList', 'tagMetaInfo', 'payload', 'globalTag', 'tagSize' or 'globalTagMap' instead")
    elif args.command in ["remove", "create", "store", "clone"]:
        # Implement handling for 'remove', 'create', 'store' commands based on args.type
        if args.command == "remove":
            if args.type == "tag":
                remove_tag_command(args, unknown_args)
            elif args.type == "globalTag":
                remove_global_tag_command(args, unknown_args)
            elif args.type == "globalTagMap":
                remove_global_tag_map_command(args, unknown_args)
            else:
                print(f"Unsupported 'remove' command type: {args.type}, use 'tag', 'globalTag' or 'globalTagMap' instead")
        elif args.command == "create":
            if args.type == "tag":
                create_tag_command(args, unknown_args)
            elif args.type == "tagMetaInfo":
                create_tag_meta_info_command(args, unknown_args)
            elif args.type == "globalTag":
                create_global_tag_command(args, unknown_args)
            elif args.type == "globalTagMap":
                create_global_tag_map_command(args, unknown_args)
            else:
                print(f"Unsupported 'create' command type: {args.type}, use 'tag', 'tagMetaInfo', 'globalTag' or 'globalTagMap' instead")
        elif args.command == "store":
            if args.type == "payload":
                store_payload_command(args, unknown_args)
            else:
                print(f"Unsupported 'store' command type: {args.type}, use 'payload' instead")
        elif args.command == "clone":
            if args.type == "globalTagMap":
                clone_global_tag_map_command(args, unknown_args)
            elif args.type == "tag":
                clone_tag_command(args, unknown_args)
            elif args.type == "iov":
                clone_iov_command(args, unknown_args)
            else:
                print(f"Unsupported 'clone' command type: {args.type}, use 'globalTagMap' instead")        
                pass
    else:
        print(f"Unsupported command: {args.command}")

#-----------------------------------


if __name__ == '__main__':
    main()
