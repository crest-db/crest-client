import json
import base64
from typing import Dict, List, Any
from hep.crest.client.models import StoreDto, StoreSetDto

class CrestCondContainer:
    """
    Utility class for the Crest API.
    This class is a container for the payload data and its specification.
    It is used to create a payload for the Crest API.
    The payload is a dictionary with the channel id as key and the data array as value.
    """
    def __init__(self, payload_spec: Dict[str, str]) -> None:
        """
        Initialize the CrestCondContainer with the payload specification.

        Args:
            payload_spec (Dict[str, str]): A dictionary representing the payload specification.
        """
        self._payload_spec = payload_spec
        print(f'Payload spec: {self._payload_spec}')
        self._record: Dict[str, Any] = {}  # Container for a single record
        self._data: Dict[str, List[Any]] = {}  # Container for data arrays
        self._iov_list: List[Dict[str, Any]] = []  # Container for list of IOVs (Insertion Order Values)

    def add_record(self, attribute_list: Dict[str, Any]) -> Dict[str, Any]:
        """
        Add a record to the container.

        Args:
            attribute_list (Dict[str, Any]): A dictionary representing the attributes of the record.

        Returns:
            Dict[str, Any]: The added record.
        """
        if len(attribute_list) != len(self._payload_spec):
            raise ValueError('Attribute list does not match the payload spec')
        for key, value in attribute_list.items():
            if key not in self._payload_spec:
                raise ValueError(f'Attribute {key} is not in the payload spec')
            if 'String' in self._payload_spec[key]:
                self._record[key] = value
            if 'Blob' in self._payload_spec[key]:
                self._record[key] = base64.b64encode(value).decode('utf-8') if isinstance(value, (bytes, bytearray)) else value
            if 'Int' in self._payload_spec[key]:
                self._record[key] = int(value)
            if 'Float' in self._payload_spec[key] or 'Double' in self._payload_spec[key]:
                self._record[key] = float(value)
        return self._record

    def add_data(self, channel_id: str, record: Dict[str, Any]) -> Dict[str, List[Any]]:
        """
        Add a record to the data array.

        Args:
            channel_id (str): The channel id for the record.
            record (Dict[str, Any]): The record to add.

        Returns:
            Dict[str, List[Any]]: The updated data dictionary.
        """
        self._data[channel_id] = [value for value in record.values()]
        return self._data

    def get_payload(self) -> Dict[str, Any]:
        """
        Get the payload.

        Returns:
            Dict[str, Any]: The payload.
        """
        return {'data': self._data}

    def add_iov(self, since: int, data: Dict[str, Any], streamer_info: Dict[str, Any]) -> List[Dict[str, Any]]:
        """
        Add an IOV (Insertion Order Value) to the list.

        Args:
            since (int): The timestamp indicating the start of validity for the data.
            data (str): The data associated with the IOV.
            streamer_info (str): Additional streamer information.

        Returns:
            List[Dict[str, Any]]: The updated list of IOVs.
        """
        entry = {'since': since, 'data': json.dumps(data), 'streamerInfo': json.dumps(streamer_info)}
        self._iov_list.append(entry)
        return self._iov_list

    def flush_iovs(self) -> List[StoreDto]:
        """
        Dump the list of IOVs and clear it.

        Returns:
            List[StoreDto]: The list of dumped IOVs.
        """
        out_iov_list = []
        # convert to StoreDto objects
        for iov in self._iov_list:
            storedto = StoreDto(
                since=int(iov['since']),
                data=iov['data'],
                streamer_info=iov['streamerInfo']
            )
            out_iov_list.append(storedto)
        self._iov_list = []
        return out_iov_list

    def get_store_set(self) -> StoreSetDto:
        """
        Create a storeset for payload uploads
        """
        storedto_list = self.flush_iovs()
        storeset = StoreSetDto(
            size=len(storedto_list),
            datatype="iovs",
            format="StoreSetDto",
            resources=storedto_list
        )
        return storeset

    def get_iov_list_size(self) -> int:
        """
        Get the size of the list of IOVs.

        Returns:
            int: The size of the list of IOVs.
        """
        return len(self._iov_list)
