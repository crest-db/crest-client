from typing import Dict, Any, Union, List, Optional
import json
import os
import requests
from datetime import datetime
from hep.crest.client import Configuration, ApiClient, ApiException
from hep.crest.client.api import iovs_api, admin_api, globaltags_api, globaltagmaps_api, payloads_api, tags_api, runinfo_api
from hep.crest.client.models import (
    IovSetDto, HTTPResponse, TagMetaSetDto, TagMetaDto, TagSetDto, TagDto, GlobalTagDto, 
    GlobalTagSetDto, GlobalTagMapDto, StoreSetDto, StoreDto, RunLumiInfoDto, RunLumiSetDto
)

class CrestApi():
    _config = None
    _host = None
    _api_client = None

    def __init__(self, host: str):
        self._host = host
        self._config = Configuration(host=host)
        self._api_client = ApiClient(self._config)

    def socks(self, url: str = 'localhost:3129') -> None:
        SOCKS5_PROXY_HOST = url.split(':')[0]
        SOCKS5_PROXY_PORT = int(url.split(':')[1])
        try:
            import socket
            import socks  # you need to install pysocks (use the command: pip install pysocks)
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
        except:
            print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

    @staticmethod
    def build_params(kwargs: Dict[str, Any]) -> Dict[str, Any]:
        # Verify your data
        return kwargs

    def select_groups(self, name: str, snapshot: Optional[float] = None, size: int = 1000, page: int = 0, sort: str = 'id.since:ASC') -> Union[IovSetDto, HTTPResponse]:
        """
        List iov groups for a given tagname
        :param name: Name of the tag
        :param snapshot: Snapshot time  
        :param size: Number of results per page
        :param page: Page number    
        :param sort: Sorting order
        :return: IovSetDto if successful, else HTTPResponse 

        This method is the equivalent of CrestApi.selectGroups() in the C++ code.
        """
        criteria = {
            'snapshot': snapshot,
            'page': page,
            'size': size,
            'sort': sort
        }
        api_instance = iovs_api.IovsApi(self._api_client)
        try:
            api_response = api_instance.find_all_iovs(tagname=name, method="GROUPS", **criteria)
            return api_response
        except ApiException as e:
            print("Exception when calling IovsApi->find_all_iovs: %s\n" % e)

    def select_iovs(self, name: str, since: int = 0, until: int = 0, snapshot: Optional[float] = None,
                    timeformat: str = 'NUMBER', size: int = 1000, page: int = 0,
                    sort: str = 'id.since:ASC', x_crest_query='IOVS') -> Union[IovSetDto, HTTPResponse]:
        """
        List iov for a given tagname using a range in time
        :param tagname: Name of the tag
        :param since: Start time
        :param until: End time
        :param snapshot: Snapshot time
        :param timeformat: Format of time
        :param size: Number of results per page
        :param page: Page number
        :param sort: Sorting order
        :return: IovSetDto if successful, else HTTPResponse

        This method is the equivalent of CrestApi.selectIovs() in the C++ code.
        The field timeformat is not used in the C++ code, as it defaults to 'NUMBER'.
        """
        criteria = {
            'snapshot': snapshot,
            'since': since,
            'until': until,
            'timeformat': timeformat,
            'page': page,
            'size': size,
            'sort': sort,
            'x_crest_query': 'IOVS'
        }
        api_instance = iovs_api.IovsApi(self._api_client)
        try:
            api_response = api_instance.find_all_iovs(tagname=name, method="IOVS", **criteria)
            return api_response
        except ApiException as e:
            print("Exception when calling IovsApi->find_all_iovs: %s\n" % e)

    def find_all_iovs(self, tagname: str, **kwargs: Dict[str, Any]) -> Union[IovSetDto, HTTPResponse]:
        """
        List all iovs for a given tagname.
        
        :param tagname: Name of the tag.
        :param kwargs: Additional criteria.
            - since: number representing the since value.
            - until: number representing the until value.
            - snapshot: number for snapshot time.
            - method: string for method [IOVS, GROUPS, MONITOR].
            - timeformat: string for timeformat [NUMBER, MS, ISO, RUN, RUN_LUMI, CUSTOM].
            - groupsize: number for groupsize.
            - page: number for page.
            - size: number for size of the page.
        :return: IovSetDto if successful, else HTTPResponse.

        This method is not exposed by CrestApi in the C++ code.
        """
        criteria = CrestApi.build_params(kwargs)
        params = {
            'method': 'IOVS',
            'timeformat': 'NUMBER',
            'page': 0,
            'size': 1000
        }
        params.update(criteria)
        api_instance = iovs_api.IovsApi(self._api_client)
        try:
            api_response = api_instance.find_all_iovs(tagname=tagname, **params)
            return api_response
        except ApiException as e:
            print("Exception when calling IovsApi->find_all_iovs: %s\n" % e)

    def store_iovs(self, iov_set_dto: IovSetDto) -> Any:
        """
        This method stores a set of iovs in a tag. 
        The iovs have to be in the IovDto object.

        :param iov_set_dto (IovSetDto): IovSetDto object, which contains iovs, since and hash parameters
        :return: API response
        """
        if iov_set_dto is None:
            raise ValueError("IovSetDto object is required.")
        
        api_instance = iovs_api.IovsApi(self._api_client)
        try:
            return api_instance.store_iov_batch(iov_set_dto=iov_set_dto)
        except ApiException as e:
            print(f"Exception when calling IovsApi->store_iovs: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def list_tags(self, name: str, size: int = 1000, page: int = 0, sort: str = 'name:ASC', **kwargs: Dict[str, Any]) -> Union[TagSetDto, HTTPResponse]:
        """
        List all tags.
        :param name: Name of the tag.
        :param size: Number of results per page.
        :param page: Page number.
        :param sort: Sorting order.
        :param kwargs: Additional criteria.
            - description: string for description.
            - timeType: selection for timeType [time, run, run-lumi].
            - objectType: selection for objectType (payloadSpec).
        :return: TagSetDto if successful, else HTTPResponse.

        This method is the equivalent of CrestApi.listTags() in the C++ code.
        """
        criteria = CrestApi.build_params(kwargs)
        params = {
            'page': page,
            'size': size,
            'sort': sort
        }
        if name != 'none':
            params['name'] = name
        params.update(criteria)
        api_instance = tags_api.TagsApi(self._api_client)
        try:
            api_response = api_instance.list_tags(**params)
            return api_response
        except ApiException as e:
            print("Exception when calling TagsApi->list_tags: %s\n" % e)

    def find_global_tag(self, name: str) -> Union[GlobalTagSetDto, HTTPResponse]:
        """
        Find a global tag with the given name.

        :param name: The name of the global tag to find.
        :return: GlobalTagSetDto if successful, else HTTPResponse.

        This method is the equivalent of CrestApi.findGlobalTag() in the C++ code.
        """
        api_instance = globaltags_api.GlobaltagsApi(self._api_client)
        try:
            api_response = api_instance.find_global_tag(name=name)
            return api_response
        except ApiException as e:
            print("Exception when calling GlobaltagsApi->find_global_tag: %s\n" % e)

    def list_global_tags(self, name: str, size: int = 1000, page: int = 0, sort: str = 'name:ASC', **kwargs: Dict[str, Any]) -> Union[GlobalTagSetDto, HTTPResponse]:
        """
        List all global tags.

        :param name: Name of the global tag.
        :param size: Number of results per page.
        :param page: Page number.
        :param sort: Sorting order.
        :param kwargs: Additional criteria for filtering the global tags.
            - description: string for description
            - release: string for release
            - scenario: string for scenario
        :return: GlobalTagSetDto if successful, else HTTPResponse.

        This method is the equivalent of CrestApi.listGlobalTags() in the C++ code.
        """
        criteria = CrestApi.build_params(kwargs)
        params = {
            'page': page,
            'size': size,
            'sort': sort
        }
        if name != 'none':
            params['name'] = name
        params.update(criteria)
        api_instance = globaltags_api.GlobaltagsApi(self._api_client)
        try:
            api_response = api_instance.list_global_tags(**params)
            return api_response
        except ApiException as e:
            print("Exception when calling GlobaltagsApi->list_global_tags: %s\n" % e)

    def find_global_tag_map(self, name: Optional[str] = None, mode: str = 'Trace'):
        """
        Find all global tag maps.

        :param name: Name of the global tag or tag.
        :param mode: Map mode [Trace, BackTrace].
        :return: Response from the API.

        This method is the equivalent of CrestApi.findGlobalTagMap() in the C++ code.
        """
        headers = {'x_crest_map_mode': mode}
        api_instance = globaltagmaps_api.GlobaltagmapsApi(self._api_client)
        try:
            return api_instance.find_global_tag_map(name=name, **headers)
        except ApiException as e:
            print(f"Exception when calling GlobaltagmapsApi->find_global_tag_map: {e}\n")

    def create_tag(self, dto: TagDto) -> Union[TagDto, HTTPResponse]:
        """
        Create a new tag.

        :param dto: The TagDto.
        :return: Created tag or HTTP response if unsuccessful.

        This method is the equivalent of CrestApi.createTag() in the C++ code.
        """
        
        api_instance = tags_api.TagsApi(self._api_client)
        try:
            return api_instance.create_tag(tag_dto=dto)
        except ApiException as e:
            print(f"Exception when calling TagsApi->create_tag: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )
        
    def create_tag_meta(self, dto: TagMetaDto) -> Union[TagMetaDto, HTTPResponse]:
        """
        Create the meta information for a tag.

        :param dto: The TagMetaDto.
        :return: Created tag meta or HTTP response if unsuccessful.

        This method is the equivalent of CrestApi.createTagMeta() in the C++ code.
        """
        api_instance = tags_api.TagsApi(self._api_client)
        try:
            return api_instance.create_tag_meta(name=dto.tag_name, tag_meta_dto=dto)
        except ApiException as e:
            print(f"Exception when calling TagsApi->create_tag_meta: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def update_tag_meta(self,  dto: TagMetaDto) -> Union[TagMetaDto, HTTPResponse]:
        """
        Update the meta information for a tag.

        :param dto: The TagMetaDto for update.
        :return: Updated tag meta or HTTP response if unsuccessful.

        This method is the equivalent of CrestApi.updateTagMeta() in the C++ code.        
        """

        api_instance = tags_api.TagsApi(self._api_client)
        try:
            return api_instance.update_tag_meta(name=dto.tag_name, tag_meta_dto=dto)
        except ApiException as e:
            print(f"Exception when calling TagsApi->update_tag_meta: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )


    def remove_tag(self, name: str) -> Union[None, HTTPResponse]:
        """
        Delete a tag.

        :param name: Name of the tag to delete.
        :return: None if successful, or HTTP response if unsuccessful.

        This method is the equivalent of CrestApi.removeTag() in the C++ code.
        """
        api_instance = admin_api.AdminApi(self._api_client)
        try:
            return api_instance.remove_tag(name=name)
        except ApiException as e:
            print(f"Exception when calling Tags remove_tag: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def create_global_tag(self, dto: GlobalTagDto, force: str) -> Union[GlobalTagDto, HTTPResponse]:
        """
        Create a new global tag.

        :param dto: The GlobalTagDto.
        :return: Created global tag or HTTP response if unsuccessful.

        This method is the equivalent of CrestApi.createTagMeta() in the C++ code.        
        """        
        api_instance = globaltags_api.GlobaltagsApi(self._api_client)
        try:
            return api_instance.create_global_tag(force=force, global_tag_dto=dto)
        except ApiException as e:
            print(f"Exception when calling GlobaltagsApi->create_global_tag: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def remove_global_tag(self, name: str) -> Union[None, HTTPResponse]:
        """
        Delete a global tag.

        :param name: Name of the global tag to delete.
        :return: None if successful, or HTTP response if unsuccessful.

        This method is the equivalent of CrestApi.removeGlobalTag() in the C++ code.
        """
        api_instance = admin_api.AdminApi(self._api_client)
        try:
            return api_instance.remove_global_tag(name=name)
        except ApiException as e:
            print(f"Exception when calling AdminApi->remove_globaltag: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def create_global_tag_map(self, dto: GlobalTagMapDto) -> Union[GlobalTagMapDto, HTTPResponse]:
        """
        Create a new global tag map.

        :param globaltagname: Name of the global tag.
        :param tagname: Name of the tag.
        :param kwargs: Additional parameters for global tag map creation.
        :return: Created global tag map or HTTP response if unsuccessful.

        This method is the equivalent of CrestApi.createGlobalTagMap() in the C++ code.
        """
       
        api_instance = globaltagmaps_api.GlobaltagmapsApi(self._api_client)
        try:
            return api_instance.create_global_tag_map(global_tag_map_dto=dto)
        except ApiException as e:
            print(f"Exception when calling GlobaltagmapsApi->create_global_tag_map: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )
    def delete_global_tag_map(self, globaltagname: str, tagname: str, label: str) -> Union[None, HTTPResponse]:
        """
        Remove a tag from a global tag.

        :param globaltagname: Name of the global tag.
        :param tagname: Name of the tag.
        :param label: Label of the mapping.
        :return: None if successful, or HTTP response if unsuccessful.
        """
        api_instance = globaltagmaps_api.GlobaltagmapsApi(self._api_client)
        try:
            return api_instance.delete_global_tag_map(name=globaltagname, tagname=tagname, label=label)
        except ApiException as e:
            print(f"Exception when calling GlobaltagmapsApi->remove_global_tag_map: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def find_tag_meta(self, name: str = 'none') -> Union[TagMetaSetDto, HTTPResponse]:
        """
        Get tag meta info.

        :param name: Name of the tag.
        :return: Tag meta info.
        """
        api_instance = tags_api.TagsApi(self._api_client)   
        try:
            return api_instance.find_tag_meta(name)
        except ApiException as e:
            print(f"Exception when calling TagsApi->find_tag_meta: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def find_tag(self, name: str = 'none') -> Union[TagDto, HTTPResponse]:
        """
        Get tag.

        :param name: Name of the tag.
        :return: Tag information.
        """
        api_instance = tags_api.TagsApi(self._api_client)   
        try:
            api_response = api_instance.find_tag(name=name)
            if api_response['size'] == 1:
                return api_response['resources'][0]
            else:
                return None
        except ApiException as e:
            print("Exception when calling TagsApi->find_tag: %s\n" % e)
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def find_global_tag(self, name: str = 'none') -> Union[GlobalTagDto, HTTPResponse]:
        """
        Get global tag.

        :param name: Name of the global tag.
        :return: Global tag information.
        """
        api_instance = globaltags_api.GlobaltagsApi(self._api_client)
        try:
            api_response = api_instance.find_global_tag(name=name)
            if api_response['size'] == 1:
                return api_response['resources'][0]
            else:
                return None
        except ApiException as e:
            print("Exception when calling GlobaltagsApi->list_global_tags in get_global_tag: %s\n" % e)
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def get_size(self, tagname: str) -> int:
        """
        Get tag size.

        :param tagname: Name of the tag.
        :return: Size of the tag.
        """
        api_instance = iovs_api.IovsApi(self._api_client)

        try:
            api_response = api_instance.get_size_by_tag(tagname, _check_return_type=False)
            return api_response["resources"][0]["niovs"]
        except ApiException as e:
            print("Exception when calling IovsApi->get_size_by_tag: %s\n" % e)
            return 0
        
    def find_payload_hash(self, resources: List[Dict], given_since: float):
        """
        Find the payload hash for a given since.

        :param resources: List of IovDto objects.
        :param given_since: Given since value.
        :return: Payload hash.
        """
        for resource in resources:
            since = resource['since']
            if given_since >= since:
                if resources.index(resource) < len(resources) - 1 \
                    and given_since < resources[resources.index(resource) + 1]['since']:
                    return resource['payload_hash']
        return None

    def get_payload_as_file(self, hash: str, fout: str):
        """
        Get payload as a file.

        :param hash: Payload hash.
        :param fout: File path to save the payload.
        """
        host = self._host
        url = f"{host}/payloads/{hash}"
        fmt = "BLOB"
        criteria = {"format": fmt}

        resp = requests.get(url, params=criteria)

        if resp.status_code == 200:
            with open(fout, "wb") as local_file:
                for chunk in resp.iter_content(chunk_size=128):
                    local_file.write(chunk)
        else:
            print(f"Error: HTTP GET request failed for payload {hash}")

    def get_payload(self, hash: str):
        """
        Get payload.

        :param hash: Payload hash.
        :return: Payload content.
        """
        host = self._host
        url = f"{host}/payloads/data?hash={hash}"
        fmt = "BLOB"
        criteria = {"format": fmt}

        resp = requests.get(url, params=criteria)

        if resp.status_code == 200:
            return resp.content
        else:
            print(f"Error: HTTP GET request failed for payload {hash}")

    def store_payload(self, tag_name: str, since: int, filepath: str, filename: str,
               compression_type: str = 'none', version: str = '1', object_type: str = 'JSON',
               **kwargs: Dict[str, Any]) -> Any:
        """
        Store data: upload a single file to CREST.

        :param tag_name: Name of the tag.
        :param since: Since value.
        :param filepath: Path to the file.
        :param filename: Name of the file.
        :param compression_type: Compression type (default is 'none').
        :param version: Version of the data (default is '1').
        :param object_type: Type of the object (default is 'JSON').
        :param kwargs: Additional optional parameters.
        :return: API response.
        """
        criteria = CrestApi.build_params(kwargs)
        params = {
            'compression_type': compression_type,
            'version': version,
            'object_type': object_type,
        }
        params.update(criteria)

        file_path = os.path.join(filepath, filename)
        if not os.path.isfile(file_path):
            raise FileNotFoundError(f"File '{file_path}' not found.")

        streamer_info = params.get('streamerInfo', f'{{"filename":"{filename}"}}')
        del params['streamerInfo']

        store = StoreDto(since=since, data=f'file://{file_path}', streamerInfo=streamer_info)
        x_crest_payload_format = "FILE"
        files = [open(file_path, 'rb')]

        storeset = StoreSetDto(
            size=1,
            datatype="iovs",
            format="StoreSetDto",
            resources=[store]
        )
        self.store_data(payload_format=x_crest_payload_format, tag=tag_name, store_set=storeset, **params)

    def store_data(self, tag: str, store_set: StoreSetDto, payload_format: str = 'JSON', object_type: str = 'JSON', compression_type: str = 'none', version: str = '1', **kwargs: Dict[str, Any]) -> Any:
        """
        This method stores a set of the payloads (with additional parameters: since and streamer info). 
        The payloads and parameters have to be in the StoreSetDto object.

        :param tag: Name of the tag.
        :param store_set (StoreSetDto): StoreSetDto object, which contains payloads (in form of strings or files), since parameters for them
        :param payload_format: payload format (default is 'JSON').
        :param object_type: Description of the type (default is 'JSON').
        :param compression_type: Compression type (default is 'none').
        :param version: Version of the data (default is '1').
        :param kwargs: Additional optional parameters.
            end_time: a number (double) in the same format as since representing an end of validity propagated to the tag.
        :return: API response
        """
        if tag is None:
            raise ValueError("Tag name is required.")
        if store_set is None:
            raise ValueError("StoreSetDto object is required.")
        
        criteria = CrestApi.build_params(kwargs)
        params = {
            'compression_type': compression_type,
            'version': version,
            'object_type': object_type,
            'endtime': 0,
        }
        if 'end_time' in criteria:
            params['endtime'] = criteria['end_time']
        if payload_format == 'FILE':
            # Store as external file
            x_crest_payload_format = "FILE"
            files = []
            for item in store_set.resources:
                if item.data.startswith('file://'):
                    file_path = item.data[7:]
                    if not os.path.isfile(file_path):
                        raise FileNotFoundError(f"File '{file_path}' not found.")
                    files.append(open(file_path, 'rb'))
                    break
                else:
                    raise ValueError("No file found in the store set.")
            openapi_model = self._api_client.sanitize_for_serialization(store_set)
            # print(f'Model after sanitize: {openapi_model}')
            json_body = json.dumps(openapi_model)
            headers = {'X-Crest-PayloadFormat': x_crest_payload_format}
            api_instance = payloads_api.PayloadsApi(self._api_client)
            try:
                print(f'Uploading payload batch files {temp_file} to CREST')
                return api_instance.store_payload_batch(tag=tag, storeset=json_body,
                                                        x_crest_payload_format=x_crest_payload_format, **params,
                                                        files=files)
            except ApiException as e:
                print("Exception when calling PayloadsApi->store_payload_batch: %s\n" % e)

        elif payload_format == 'JSON':
            openapi_model = self._api_client.sanitize_for_serialization(store_set)
            ## print(f'Model after sanitize: {openapi_model}')
            
            temp_file = f".{tag}.json"
            with open(temp_file, "w") as outfile:
                json.dump(openapi_model, outfile)

            api_instance = payloads_api.PayloadsApi(self._api_client)
            try:
                print(f'Uploading JSON file {temp_file} to CREST')
                #url = self._host + '/payloads'
                #print(f'Using URL: {url}')
                #resp = self.upload_json_with_requests(url, tag, temp_file, params)
                with open(temp_file, 'rb') as file_obj:
                    resp = api_instance.upload_json(tag=tag, storeset=file_obj, **params)
                    print("Upload successful!")
                    return resp

            except ApiException as e:
                print("Exception when calling PayloadsApi->upload_json: %s\n" % e)
                raise
            finally:
                print(f"Removing temporary file {temp_file}")
                os.remove(temp_file)

    def upload_json_with_requests(self, url, tag, temp_file, params):
        # Prepare the headers and multipart form data
        headers = {
            'Accept': 'application/json',  # Adjust as needed
        }
        
        # Open the file in binary mode
        with open(temp_file, 'rb') as file_obj:
            # Multipart form data
            files = {
                'storeset': (temp_file, file_obj, 'application/json'),  # Field name and metadata
            }
            
            # Other fields in the form
            data = {
                'tag': tag,
                **params  # Add any additional parameters here
            }
            
            try:
                print(f'Uploading JSON file {temp_file} to {url}')
                response = requests.put(url, headers=headers, files=files, data=data)
                response.raise_for_status()  # Raise HTTPError for bad responses (4xx, 5xx)

                print('Upload successful!')
                return response.json()  # Assuming the server responds with JSON
            except requests.RequestException as e:
                print(f"Error during upload: {e}")
                return None

    def create_run_lumi(self, run_number: float, lumiblock_number: float,
                    start_date: float, end_date: float) -> Any:
        """
        Create a new entry for run and lumi block.

        :param run_number: Run number.
        :param lumiblock_number: Lumiblock number.
        :param start_date: Start date.
        :param end_date: End date.
        :return: API response.
        """
        rlb = RunLumiInfoDto(run_number=float(run_number),
                             lb=float(lumiblock_number),
                             starttime=float(start_date),
                             endtime=float(end_date))
        rlbset = RunLumiSetDto(
            size=1,
            datatype="runs",
            format="RunLumiSetDto",
            resources=[rlb]
        )

        api_instance = runinfo_api.RuninfoApi(self._api_client)
        try:
            return api_instance.create_run_info(run_lumi_set_dto=rlbset)
        except ApiException as e:
            print("Exception when calling RuninfoApi->create_run_info: %s\n" % e)
            return HTTPResponse(status_code=e.status, reason=e.reason, code=e.status, message=e.body)

    def list_run_lumi(self, since_time: Union[datetime, str], until_time: Union[datetime, str], 
                      format: str = 'NUMBER', mode: str = 'runrange', page: int = 0, size: int = 1000, 
                      **kwargs):
        """
        List run and lumi block.

        :param since_time: Since time. Can be either a datetime object or a string.
        :param until_time: Until time. Can be either a datetime object or a string.
        :param format: Format of time (default is 'NUMBER'). Possible values: 'NUMBER', 'ISO'.
        :param mode: Range mode (default is 'runrange'). Possible values: 'runrange', 'daterange'.
        :param page: Page number (default is 0).
        :param size: Size of the page (default is 1000).
        :param kwargs: Additional optional parameters.
        :return: API response.
        """
        criteria = CrestApi.build_params(kwargs)
        params = {
            'format': format,
            'mode': mode,
            'page': page,
            'size': size
        }
        params.update(criteria)
        api_instance = runinfo_api.RuninfoApi(self._api_client)
        try:
            api_response = api_instance.list_run_info(since=str(since_time), until=str(until_time), **params)
            return api_response
        except ApiException as e:
            print("Exception when calling RuninfoApi->list_run_info: %s\n" % e)
            return HTTPResponse(status_code=e.status, reason=e.reason, code=e.status, message=e.body)
