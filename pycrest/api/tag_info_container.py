import json
from hep.crest.client.models import TagMetaDto

class TagInfoContainer:
    """
    Utility class for the Crest API.
    This class is a container for the tag meta information.
    It is used to create a TagMetaInfo via Crest API.
    The payload specification is a dictionary with the channel id as key and the data array as value.
    """
     
    def __init__(self, tagname: str, folder: str, schema: str) -> None:
        """
        Initialize the TagInfoContainer with tag name, folder, and schema.

        Args:
            tagname (str): The name of the tag.
            folder (str): The folder associated with the tag.
            schema (str): The schema associated with the tag.
        """
        self._tag_name = tagname
        self._folder = folder
        self._schema = schema
        print(f'Tag for folder: {self._folder}, schema: {self._schema}')
        self._payload_spec = {}
        self._channel_list = []
        self._tag_info = {}

    def add_record(self, name: str, type: str) -> None:
        """
        Add a record to the payload specification.

        Args:
            name (str): The name of the record.
            type (str): The type of the record.
        """
        self._payload_spec[name] = type

    def add_channel(self, channel_id: str = '0', channel_name: str = 'none') -> None:
        """
        Add a channel to the channel list.

        Args:
            channel_id (str): The ID of the channel.
            channel_name (str): The name of the channel.
        """
        self._channel_list.append({channel_id: channel_name})

    def set_node_description(self, node_description: str) -> None:
        """
        Set the node description.

        Args:
            node_description (str): The description of the node.
        """
        self._tag_info['node_description'] = node_description
    
    def get_tag_info(self) -> dict:
        """
        Get the tag information.

        Returns:
            dict: The tag information.
        """
        payload_spec = ', '.join(f'{k}:{v}' for k, v in self._payload_spec.items())
        self._tag_info['payload_spec'] = payload_spec
        self._tag_info['channel_list'] = self._channel_list
        return self._tag_info

    def get_description(self) -> dict:
        """
        Get the tag description.

        Returns:
            dict: The tag description.
        """
        return {'schema': self._schema, 'node_fullpath': self._folder}

    def get_payload_spec(self) -> dict:
        """
        Get the payload specification.

        Returns:
            dict: The payload specification.
        """
        return self._payload_spec
    
    def get_channel_list(self) -> list:
        """
        Get the list of channels.

        Returns:
            list: The list of channels.
        """
        return self._channel_list
     
    def get_channels_size(self) -> int:
        """
        Get the number of channels.

        Returns:
            int: The number of channels.
        """
        return len(self._channel_list)
    
    def get_columns_size(self) -> int:
        """
        Get the number of columns in the payload specification.

        Returns:
            int: The number of columns.
        """
        return len(self._payload_spec.keys())
    
    def get_folder(self) -> str:
        """
        Get the folder associated with the tag.

        Returns:
            str: The folder associated with the tag.
        """
        return self._folder

    def get_schema(self) -> str:
        """
        Get the schema associated with the tag.

        Returns:
            str: The schema associated with the tag.
        """
        return self._schema
    
    def get_tag_name(self) -> str:
        """
        Get the name of the tag.

        Returns:
            str: The name of the tag.
        """
        return self._tag_name

    def get_tag_info_params(self) -> dict:
        """
        Build dictionary information for tag meta creation via API.

        Returns:
            dict: The tag meta information.
        """
        meta = {
            'description': json.dumps(self.get_description()),
            'chansize': self.get_channels_size(),
            'colsize': self.get_columns_size(),
            'tag_info': json.dumps(self.get_tag_info()),
        }
        return meta
    
    def get_tag_info_dto(self) -> TagMetaDto:
        """
        Get the TagMetaDto instance for the tag meta information.
        Returns:
            TagMetaDto: The TagMetaDto instance.
        """
        tag_meta = TagMetaDto(
            tag_name = self._tag_name,
            description = json.dumps(self.get_description()),
            chansize = self.get_channels_size(),
            colsize = self.get_columns_size(),
            tag_info = json.dumps(self.get_tag_info())
        )

        return tag_meta
