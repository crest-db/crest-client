import json

def is_json_serializable(obj):
    try:
        json.dumps(obj)
        return True
    except (TypeError, OverflowError):
        return False

class ChannelSetDto:
    channels = []
    
    def get_channels(self) -> list:
        return self.channels

    def get_size(self) -> int:
        return len(self.channels)

    def add(self,id: str,name: str) -> None:
        pair = {id:name};
        self.channels.append(pair)
        return

    def to_json(self) -> json:
        json_string = json.dumps(self.channels)
        js = json.loads(json_string)
        return js

    def from_json(self,js) -> None:
        self.channels = []
        for element in js:
            for key in element.keys():
                self.add(key,element[key])
        return
        

class PayloadSpecDto:
    row = []
    
    def get_columns(self) -> list:
        return self.row

    def get_size(self) -> int:
        return len(self.row)

    def add(self,id: str,name: str) -> None:
        pair = {id:name};
        self.row.append(pair)
        return

    def to_json(self) -> json:
        json_string = json.dumps(self.row)
        js = json.loads(json_string)
        return js

    def from_json(self,js) -> None:
        self.row = []
        for element in js:
            for key in element.keys():
                self.add(key,element[key])
        return

    

class TagInfoDto:
    node_description: str
    payload_spec: PayloadSpecDto = PayloadSpecDto()
    channel_list: ChannelSetDto = ChannelSetDto()

    def __init__(self, description: str, **kwargs):
        self.node_description = description

        if 'payload_spec' in kwargs:
            self.set_payload_spec(kwargs.get("payload_spec"))

        if 'channel_list' in kwargs:
            self.set_payload_spec(kwargs.get("channel_list"))

    def set_payload_spec(self, spec: PayloadSpecDto) -> None:
        if type(spec) is PayloadSpecDto:
            self.payload_spec = spec
            return
        else:
            raise TypeError('Error in method TagInfoDto::set_payload_spec: Wrong type error for payload specification')

    def set_channel(self, ch: ChannelSetDto) -> None:
        if type(ch) is ChannelSetDto:
            self.channel_list = ch
            return
        else:
            raise TypeError('Error in method TagInfoDto::set_channel: Wrong type error for channel set')
    
    def get_payload_spec(self) -> PayloadSpecDto:   
        return self.payload_spec

    def get_channel(self) -> ChannelSetDto:
        return self.channel_list

    def get_folder_description(self) -> str:
        return self.node_description;

    def get_channel_size(self) -> int:
        return channel_list.get_size()

    def get_column_size(self) -> int:
        return payload_spec.get_size()

    def to_json(self) -> json:
        js = json.loads("{}")
        js["node_description"] = self.node_description
        js["payload_spec"] = json.dumps(self.payload_spec.to_json());
        js["channel_list"] = json.dumps(self.channel_list.to_json());
        return js

    def from_json(self,js) -> None:
        if 'node_description' not in js:
            raise ValueError("Error in method TagInfoDto::from_json: key node_description does not exist in JSON")
        else:
            self.node_description = js['node_description']

        if 'payload_spec' not in js:
            raise ValueError("Error in method TagInfoDto::from_json: key payload_spec does not exist in JSON")
        else:
            self.payload_spec.from_json(json.loads(js['payload_spec']))

        if 'channel_list' not in js:
            raise ValueError("Error in method TagInfoDto::from_json: key channel_list does not exist in JSON")
        else:
            self.channel_list.from_json(json.loads(js['channel_list']))
            
        return

