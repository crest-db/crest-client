from typing import Dict, Any, Union, List, Optional
import json
import os

from datetime import datetime

from hep.crest.client import ApiException
from hep.crest.client.api import iovs_api, admin_api, globaltags_api, globaltagmaps_api, payloads_api, tags_api, runinfo_api
from hep.crest.client.models import (
    IovSetDto, HTTPResponse, TagMetaSetDto, TagMetaDto, TagSetDto, TagDto, GlobalTagDto, 
    GlobalTagSetDto, GlobalTagMapDto, GlobalTagMapSetDto, StoreSetDto, StoreDto,
    RunLumiInfoDto, RunLumiSetDto, PayloadDto, IovDto
)


import pycrest.api.crest_fs.crest_fs_config as conf
from pycrest.api.crest_fs.crest_fs_utils import read_file, write_file, check_directory, get_hash, compute_sha256, utf8len
import shutil
from pycrest.cli.commands.utils import datetime_serializer
from hep.crest.client.model_utils import model_to_dict, validate_and_convert_types


import ast

class CrestApiFs():
    _config = None
    _dir = "/tmp/crest_dump"
    _api_client = None

    def __init__(self, dir: str):
        self._dir = dir


    def print_dir(self):
        print (self._dir)

    @staticmethod
    def build_params(kwargs: Dict[str, Any]) -> Dict[str, Any]:
        # Verify your data
        return kwargs
        

    def create_tag(self, dto: TagDto) -> Union[TagDto, HTTPResponse]:
        """
        Create a new tag.

        :param dto (TagDto): tag.
        :return: Created tag or HTTP response if unsuccessful.
        """

        name = dto.name
        path = self._dir + conf.fs_tag_path + "/" + name

        try:
            check_directory(path);
            file = path + conf.fs_tag_file
            tagjs = model_to_dict(dto)
            str1 = json.dumps(tagjs)
            write_file(file,str1)
            return dto
        except ApiException as e:
            print(f"Exception in method CrestApiFs->create_tag: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )



    def create_tag_meta(self, dto: TagMetaDto) -> Union[TagMetaDto, HTTPResponse]:
        """
        Create the meta information for a tag.

        :param dto (TagMetaDto): tag meta info.
        :return: Created tag meta or HTTP response if unsuccessful.
        """

        name = dto.tag_name
        path = self._dir + conf.fs_tag_path + "/" + name
        
        try:
            check_directory(path);
            file = path + conf.fs_tagmetainfo_file
            t_meta = model_to_dict(dto)
            str1 = json.dumps(t_meta)
            write_file(file,str1)
            return dto
        except ApiException as e:
            print(f"Exception in method CrestApiFs->create_tag_meta: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )
        


    def store_payload(self, tag_name: str, since: int,  payload: StoreDto, compression_type: str = 'none', version: str = '1',
                       object_type: str = 'JSON', payload_format: str = 'JSON', streamer_info: str = 'none', **kwargs: Dict[str, Any]) -> Any:
        """
        store data: uploads a single payload as a file/string to CREST. The auxiliary method.

        :param tag_name: Name of the tag.
        :param since: Since value.
        :param payload: payload of file with the payload.
        :param compression_type: Compression type (default is 'none').
        :param version: Version of the data (default is '1').
        :param payload_format: Type of the object (default is 'JSON').
        :param kwargs: Additional optional parameters.
        :return: API response.
        """

        if (payload_format == 'FILE') :
            file_path = payload.replace("file://", "")
            if not os.path.isfile(file_path):
                raise FileNotFoundError(f"File '{file_path}' not found.")


            hash = compute_sha256(file_path)
            prefix = hash[:conf.fs_prefix_length]
        else:
            hash = get_hash(payload)
            prefix = hash[:conf.fs_prefix_length]      

        try:
            payload_path = self._dir + conf.fs_data_path + "/" + prefix + "/" + hash
            check_directory(payload_path)
            payload_file = payload_path + conf.fs_payload_file

            if (payload_format == 'FILE') :
                shutil.copyfile(file_path, payload_file)
                file_size = os.path.getsize(file_path)
            else:
                write_file(payload_file,payload)
                file_size = utf8len(payload)

            now = datetime.now()
            date_string = now.strftime("%Y-%m-%d %H:%M:%S")
  
            tag_meta = {
                "checkSum": "SHA-256",
                "compressionType": "none",
                "hash": hash,
                "insertionTime": now,
                "objectType": object_type,
                "size": file_size,
                "version": version
            }

  
            jsres = json.dumps(tag_meta, default=datetime_serializer)

            meta_file = payload_path + conf.fs_meta_file
            write_file(meta_file,jsres)

            iov = {
                "insertionTime": now,
                "payloadHash": hash,
                "since": since,
                "tagName": tag_name
            }

            siov = json.dumps(iov, default=datetime_serializer)
            iov = json.loads(siov)

            iov_path = self._dir + conf.fs_tag_path + "/" + tag_name
            iov_file = iov_path + conf.fs_iov_file

            if os.path.isfile(iov_file):
                iovs = read_file(iov_file)
                iov_list = json.loads(iovs)
                for element in iov_list:
                    if 'since' in element:
                        current_since = element['since']
                        if since == current_since:
                            iov_list.remove(element)

                iov_list.append(iov)
                iovs = json.dumps(iov_list)
                write_file(iov_file,iovs)
            else:
                iov_list = json.loads("[]")
                iov_list.append(iov)
                iovs = json.dumps(iov_list)
                write_file(iov_file,iovs)

        except ApiException as e:
            print("Exception in method CrestApiFs->store_payload: %s\n" % e)
            
            

    def store_data(self, tag: str, store_set: StoreSetDto, payload_format: str = 'JSON', object_type: str = 'JSON', compression_type: str = 'none', version: str = '1', **kwargs: Dict[str, Any]) -> Any:
        """
        This method stores a set of the payloads (with additional parameters: since and streamer info) on the file storage. 
        The payloads and parameters have to be in the StoreSetDto object.

        :param tag: Name of the tag.
        :param store_set (StoreSetDto): StoreSetDto object, which contains payloads (in form of strings or files), since parameters for them
        :param payload_format: payload format (default is 'JSON').
        :param object_type: Description of the type (default is 'JSON').
        :param compression_type: Compression type (default is 'none').
        :param version: Version of the data (default is '1').
        :param kwargs: Additional optional parameters.
        :return: API response
        """
        data = str(store_set)
        dto = store_set.to_dict()

        res = store_set.resources
        n_elem = len(res)

        for elem in res:
            item = elem.to_dict()
            
            try:
                payload = elem.data
                since = int(elem.since)
                streamer_info = elem.streamer_info

                self.store_payload( tag, since,  payload, payload_format = payload_format, streamer_info = streamer_info)
                
            except Exception as e:
                print('Error' % e)

                
    def find_tag(self, name: str) -> Union[TagDto, HTTPResponse]:
        """
        This method returns a tag for the tag name.

        :param name (str): tag name.
        :return: tag (as TagDto) or HTTP response if unsuccessful.
        """

        path = self._dir + conf.fs_tag_path + "/" + name

        try:
            file = path + conf.fs_tag_file

            result = read_file(file) # str
            result = json.loads(result)

            dto = validate_and_convert_types(
                result,
                {dict, TagDto},
                ['result'],
                True,
                True,
                TagDto
            )
            
            return dto
        except ApiException as e:
            print(f"Exception in method CrestApiFs->find_tag: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )


    def find_tag_meta(self, name: str) -> Union[TagMetaDto, HTTPResponse]:
        """
        This method returns a tag meta info for the tag name.

        :param name (str): tag name.
        :return: tag (as TagDto) or HTTP response if unsuccessful.
        """

        path = self._dir + conf.fs_tag_path + "/" + name

        try:
            file = path + conf.fs_tagmetainfo_file

            result = read_file(file) # str
            result = json.loads(result)

            dto = validate_and_convert_types(
                result,
                {dict, TagMetaDto},
                ['result'],
                True,
                True,
                TagMetaDto
            )
            
            return dto
        except ApiException as e:
            print(f"Exception in method CrestApiFs->find_tag_meta: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    def get_payload(self, hash: str) -> Union[str, HTTPResponse]:
        """
        This methods returns a payload by the hash.

        :param hash     (str): hash    .
        :return: payload (as string) or HTTP response if unsuccessful.
        """
        
        try:
            prefix = hash[:conf.fs_prefix_length] 
            payload_path = self._dir + conf.fs_data_path + "/" + prefix + "/" + hash
            payload_file = payload_path + conf.fs_payload_file

            result = read_file(payload_file) # str
            
            return result
        except ApiException as e:
            print(f"Exception in method CrestApiFs->get_payload: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )


    def get_payload_meta(self, hash: str) -> Union[PayloadDto, HTTPResponse]:
        """
        This method returns a payload meta info for the hash.

        :param hash     (str): hash    .
        :return: payload meta (as PayloadDto) or HTTP response if unsuccessful.
        """
        
        try:
            prefix = hash[:conf.fs_prefix_length] 
            payload_path = self._dir + conf.fs_data_path + "/" + prefix + "/" + hash
            meta_file = payload_path + conf.fs_meta_file

            result = read_file(meta_file) # str
            result = json.loads(result)

            dto = validate_and_convert_types(
                result,
                {json, dict, PayloadDto},
                ['result'],
                True,
                True,
                PayloadDto
            )
            
            return dto
        except ApiException as e:
            print(f"Exception in method CrestApiFs->get_payload_meta: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )



    def sort_iov_json(self, data: json, order: bool):
        """
        The auxiliary method to sort the JSON array with the IOVs by the since parameters.

        :param json  : JSON array with the IOVs (IOV list).
        :param order : sorting order ( True - ascending order, False - descending)
        :return: sorted JSON array with the IOV list.
        """
        
        res = json.loads("[]")
        unsorted = {}

        for element in data:
            if element.get('since') is not None:
                since = element['since']
                unsorted[since] = element
            else:
                raise Exception("JSON has no since parameter")
        if order:
            sorted_dict = dict(sorted(unsorted.items(),reverse = False))
        else:
            sorted_dict = dict(sorted(unsorted.items(),reverse = True))

        for key in sorted_dict:
            res.append(sorted_dict[key])

        return res


    
    def get_page(self, data: json, size: int, page: int):
        """
        The auxiliary method to extract a subarray from JSON array.

        :param json  : JSON array to extract subarray.
        :param size: Number of item per page
        :param page: Page number
        :return: JSON subarray.
        """
        
        res = json.loads("[]")
        data_size = len(data)

        if data_size == 0:
            # no elements to return
            return res

        # index interval to load the data from JSON array:
        kmin = size * page
        kmax = size * (page + 1)

        # check if the interval is correct:
        if kmin > (data_size - 1): # out of range
            return res

        if kmax > (data_size - 1):
            kmax = data_size

        for i in range (kmin , kmax):
            res.append(data[i])
    
        return res



    def extract_interval(self, data: json, since: int, until: int):
        """
        The auxiliary method to extract an IOV list in the time interval since-until.

        :param json  : JSON array with the IOV list.
        :param since: Start time
        :param until: End time, (value -1 means "infinity")
        :return: IOV list (JSON array).
        """
        res = json.loads("[]")

        for element in data:
            if element.get('since') is not None:
                time = element['since']
            
                if until == -1:  # infinity
                    if time >= since:
                        res.append(element)
                else:
                    if (time >= since) and (time <= until):
                        res.append(element)
            else:
                raise Exception("JSON has no since parameter")
    
        return res    


    
    def select_iovs(self, name: str,
                    since: int = 0, until: int = -1,
                    size: int = 1000, page: int = 0,
                    sort: str = 'id.since:ASC') -> Union[IovSetDto, HTTPResponse]:
        """
        Find an IOV list by the tag name.

        :param name (str): Tag name.
        :param since: Start time
        :param until: End time
        :param size: Number of results per page
        :param page: Page number
        :param sort: Sorting order ('id.since:ASC' or 'id.since:DESC')
        :return: tag (as  IovSetDto) or HTTP response if unsuccessful.
        """

        path = self._dir + conf.fs_tag_path + "/" + name

        try:
            file = path + conf.fs_iov_file

            result = read_file(file) # str
            result = json.loads(result)

            # IOV list ordering:
            if sort == 'id.since:ASC':
                result = self.sort_iov_json(result, True)
            else:
                result = self.sort_iov_json(result, False)
                
            # IOV list time interval:
            result = self.extract_interval(result, since=since, until=until)

            # page extracting:
            result = self.get_page(result, size=size, page=page)

            iov_list = IovSetDto(
                size = len(result),
                format = "IovSetDto",
                datatype = "iovs",
                resources= []
            )
            
            for element in result:
                dto = validate_and_convert_types(
                    element,
                    {IovDto},
                    ['element'],
                    True,
                    True,
                    IovDto
                 )

                iov_list.resources.append(dto)

            return iov_list
        except ApiException as e:
            print(f"Exception in method CrestApiFs->select_iovs: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

    

    def create_global_tag(self, dto: GlobalTagDto) -> Union[GlobalTagDto, HTTPResponse]:
        """
        Create the global tag.

        :param dto (GlobalTagDto): global tag.
        :return: Created global tag (GlobalTagDto) or HTTP response if unsuccessful.
        """

        name = dto.name
        path = self._dir + conf.fs_globaltag_path + "/" + name
        
        try:
            check_directory(path);
            file = path + conf.fs_globaltag_file
            gtag = model_to_dict(dto)
            str1 = json.dumps(gtag)
            write_file(file,str1)
            return dto
        except ApiException as e:
            print(f"Exception in method CrestApiFs->create_global_tag: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )

        
    def find_global_tag(self, name: str) -> Union[GlobalTagDto, HTTPResponse]:
        """
        This metod returns the global tag for the given global tag name.

        :param name : global tag name.
        :return: global tag (GlobalTagDto) or HTTP response if unsuccessful.
        """
        path = self._dir + conf.fs_globaltag_path + "/" + name
        
        try:

            file = path + conf.fs_globaltag_file
            result = read_file(file) # str
            result = json.loads(result)

            dto = validate_and_convert_types(
                result,
                {dict, GlobalTagDto},
                ['result'],
                True,
                True,
                GlobalTagDto
            )
            
            return dto
        except ApiException as e:
            print(f"Exception in method CrestApiFs->find_global_tag: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )


    def check_global_tag_map(self, dto: GlobalTagMapDto):
        """
        The auxiliary method to check if the global tag and tag exist.

        :param dto (GlobalTagMapDto): global tag map.
        """

        if dto.get('global_tag_name') is not None:
            global_tag = dto['global_tag_name']
            global_tag_path = self._dir + conf.fs_globaltag_path + "/" + global_tag + conf.fs_globaltag_file

            if not os.path.isfile(global_tag_path):
                raise FileNotFoundError(f"Global tag '{global_tag}' does not exist.")
        else : 
            raise Exception("Error: globalTagName not found in JSON.")


        if dto.get('tag_name') is not None:
            tag = dto['tag_name']
            tag_path = self._dir + conf.fs_tag_path + "/" + tag + conf.fs_tag_file

            if not os.path.isfile(tag_path):
                raise FileNotFoundError(f"Tag '{tag}' does not exist.")
        else :
            raise Exception("Error: tagName not found in JSON.")
        
        
        
    def create_global_tag_map(self, dto: GlobalTagMapDto) -> Union[GlobalTagMapDto, HTTPResponse]:
        """
        This method creates a new global tag map on the file storage.

        :param dto (GlobalTagMapDto): global tag.
        :return: Created global tag map or HTTP response if unsuccessful.
        """
        inserted = False
        
        try:
            self.check_global_tag_map(dto=dto)
            global_tag = dto['global_tag_name']
            global_tagmap_path = self._dir + conf.fs_globaltag_path + "/" + global_tag + conf.fs_map_file

            if os.path.isfile(global_tagmap_path):
                # tag = dto['tagName']
                tag = dto['tag_name']
                tag_map = read_file(global_tagmap_path)
                tagmap = json.loads(tag_map)
                for element in tagmap:
                    current_tag = element['tagName']
                    if current_tag == tag:
                        tagmap.remove(element)
                        map = model_to_dict(dto)
                        tagmap.append(map)
                        map_string = json.dumps(tagmap)
                        write_file(global_tagmap_path,map_string)
                        inserted = True
                if inserted == False:
                    map = model_to_dict(dto)
                    tagmap.append(map)
                    map_string = json.dumps(tagmap)
                    write_file(global_tagmap_path,map_string)
                
            else:
                tagmap = json.loads("[]")
                map = model_to_dict(dto)
                tagmap.append(map)
                map_string = json.dumps(tagmap)
                write_file(global_tagmap_path,map_string)           

            return dto
        
        except ApiException as e:
            print(f"Exception in method CrestApiFs->create_global_tag_map: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )



    def find_global_tag_map(self, name: str) -> Union[GlobalTagMapSetDto, HTTPResponse]:
        """
        This method returns a tag for the tag name.

        :param ame (str): global tag name.
        :return: tag (as TagDto) or HTTP response if unsuccessful.
        """

        file = self._dir + conf.fs_globaltag_path + "/" + name + conf.fs_map_file
        
        try:
        
            result = read_file(file) # str
            result = json.loads(result)

            dto = GlobalTagMapSetDto(
                size = len(result),
                format = 'GlobalTagMapSetDto',
                datatype = 'maps',
                resources= []
            )

            for element in result:
                map = validate_and_convert_types(
                    element,
                    {GlobalTagMapDto},
                    ['element'],
                    True,
                    True,
                    GlobalTagMapDto
                 )

                dto.resources.append(map)
            
            return dto

        except ApiException as e:
            print(f"Exception in method CrestApiFs->find_global_tag_map: {e}\n")
            return HTTPResponse(
                status_code=e.status,
                reason=e.reason,
                code=e.status,
                message=e.body
            )
