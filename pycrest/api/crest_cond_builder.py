from typing import Dict, List, Any
from pycrest.api.crest_cond_container import CrestCondContainer
from pycrest.api.tag_info_container import TagInfoContainer
from hep.crest.client.models import TagDto

class PayloadSpecNotDefinedError(Exception):
    """Custom exception for when payload specification is not defined."""
    pass
class ChannelDictNotDefinedError(Exception):
    """Custom  exception  when channel dictionary is not defined."""
    pass

class CrestCondBuilder:
    """
    Helper class for building tag_info and payloads containers for the Crest API.
    This class is responsible for creating instances of CrestCondContainer and TagInfoContainer.
    """
    def __init__(self, schema: str, folder: str, tagname: str) -> None:
        """
        Initialize the CrestCondBuilder with a tag name.

        Args:
            tagname (str): The name of the tag.
        """
        self._tagname = tagname
        self._folder = folder
        self._schema = schema
        self._payload_spec = None
        self._channel_dict = { }
        self._node_description = ''

    def set_payload_spec(self, payload_spec: dict) -> dict:
        """
        Set the payload specification.

        Args:
            payload_spec (dict): The payload specification.

        Returns:
            dict: The payload specification.
        """
        self._payload_spec = payload_spec
        return self._payload_spec
    
    def set_channel_dict(self, channel_dict: dict) -> dict:
        """
        Set the channel dictionary.

        Args:
            channel_dict (dict): The channel dictionary.

        Returns:
            dict: The channel dictionary.
        """
        self._channel_dict = channel_dict
        return self._channel_dict
    
    def set_node_description(self, node_description: str) -> str:
        """
        Set the node description.

        Args:
            node_description (str): The node description.

        Returns:
            str: The node description.
        """
        self._node_description = node_description
        return self._node_description

    def build_tag(self, tag_description: str, time_type: str, p_dict: Dict[str, Any] = {}) -> TagDto:
        """
        Build a TagDto instance.

        Args:
            tag_description (str): The description for this tag.
            time_type (str): The type of time used (time, run-lumi,...).
            p_dict (Dict[str, Any]): Additional arguments.

        Returns:
            TagDto: The created TagDto instance.
        """

        params = {
            'payload_spec': 'crest-json-single-iov',
            'synchronization': 'none',
            'last_validated_time': -1,
            'end_of_validity': -1,
        }
        for k, v in p_dict.items():
            if (k not in params):
                raise ValueError("Invalid parameter: %s" % k)
            params[k] = v

        tagdto = TagDto(
            name=self.get_tagname(),
            description=tag_description,
            time_type=time_type,
            payload_spec=params['payload_spec'],
            synchronization=params['synchronization'],
            last_validated_time=params['last_validated_time'],
            end_of_validity=params['end_of_validity']
        )

        return tagdto


    def build_tag_info(self) -> TagInfoContainer:
        """
        Build a TagInfoContainer instance.

        Args:
            folder (str): The folder associated with the tag.
            schema (str): The schema associated with the tag.

        Returns:
            TagInfoContainer: The created TagInfoContainer instance.
        """
        if self._payload_spec is None:
            raise PayloadSpecNotDefinedError("Payload specification is not defined.")
        
        if self._channel_dict is None:
            raise ChannelDictNotDefinedError("Channel dictionary is not defined.")
        
        self.TagInfoContainer = TagInfoContainer(self._tagname, self._folder, self._schema)
        # Add records to the payload specification
        for k, v in self._payload_spec.items():
            self.TagInfoContainer.add_record(name=k, type=v)
        # Set the node description
        self.TagInfoContainer.set_node_description(self._node_description)
        # Add channels to the channel list
        for k, v in self._channel_dict.items():
            self.TagInfoContainer.add_channel(channel_id=k, channel_name=v)

        return self.TagInfoContainer
    
    def build_cond_container(self) -> CrestCondContainer:
        """
        Build a CrestCondContainer instance.

        Returns:
            CrestCondContainer: The created CrestCondContainer instance.
        """
        if self._payload_spec is None:
            raise PayloadSpecNotDefinedError("Payload specification is not defined.")
        
        self.CrestCondContainer = CrestCondContainer(self._payload_spec)
        return self.CrestCondContainer

    def init_container(self, p_spec: Dict[str, Any], n_description: str, c_dict: Dict[str, Any]) -> None:
        """
        Initialize the container.
        """
        self.set_payload_spec(p_spec)
        self.set_node_description(n_description)
        self.set_channel_dict(c_dict)
        self._tib = self.build_tag_info()
        self._crest_payload = self.build_cond_container()

    def get_tag_info(self) -> TagInfoContainer:
        """
        Get the tag info.

        Returns:
            TagInfoContainer: The tag info.
        """
        return self._tib
    
    def get_payload(self) -> CrestCondContainer:
        """
        Get the payload.

        Returns:
            CrestCondContainer: The payload.
        """
        return self._crest_payload
    
    def get_tagname(self) -> str:
        """
        Get the tag name.

        Returns:
            str: The tag name.
        """
        return self._tagname
    
    def get_folder(self) -> str:
        """
        Get the folder.

        Returns:
            str: The folder.
        """
        return self._folder
    
    def get_schema(self) -> str:
        """
        Get the schema.

        Returns:
            str: The schema.
        """
        return self._schema
    