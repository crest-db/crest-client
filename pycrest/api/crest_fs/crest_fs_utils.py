import hashlib
from pathlib import Path

def read_file(file):
    txt = Path(file).read_text()
    return txt

def write_file(file,data):
    text_file = open(file, "w")
    text_file.write(data)
    text_file.close()

def check_directory(path):
    Path(path).mkdir(parents=True, exist_ok=True)

def get_hash(string):
    # hash calculating with SHA-256 for a string
    hash_object = hashlib.sha256(string.encode())
    hex_dig = hash_object.hexdigest()
    return hex_dig

def compute_sha256(file_name):
    # hash calculating with SHA-256 for a file
    hash_sha256 = hashlib.sha256()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha256.update(chunk)
    return hash_sha256.hexdigest()

def utf8len(s):
    return len(s.encode('utf-8'))

def get_value(json, key):
    if key in json:
        return json[key]
    else:
        message = 'key ' + key + ' not found in JSON'
        raise Exception(message)



