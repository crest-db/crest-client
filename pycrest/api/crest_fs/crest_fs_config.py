fs_tag_path = "/tags"
fs_globaltag_path = "/globaltags"
fs_data_path = "/data"

fs_tag_file = "/tag.json"
fs_iov_file = "/iovs.json"
fs_tagmetainfo_file = "/tagmetainfo.json"

fs_meta_file = "/meta.json"
fs_payload_file = "/payload.json"

fs_globaltag_file = "/globaltag.json"
fs_map_file = "/maps.json"

fs_path = ""

fs_prefix_length = 3
