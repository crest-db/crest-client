
from hep.crest.client.models import (
    TagMetaDto, GlobalTagDto
)
import json

class TagManager:
    """
    Helper class for building global tag and tag associations for the Crest API.
    """
    def __init__(self, global_tag: GlobalTagDto) -> None:
        """
        Initialize the TagManager with a global_tag.

        Args:
            global_tag (GlobalTagDto): The global tag DTO.
        """
        self._global_tag = global_tag
        self._tag_list = []

    def add_tag(self, tag_meta: TagMetaDto, record: str) -> list:
        """
        Add tag for association.

        Args:
            tag_meta (TagMetaDto): The meta info of the tag.
            record (str): The record to associate the tag with.

        Returns:
            list: The tag list.
        """
        if self._global_tag.type == 'L':
            raise ValueError('Global tag is locked, cannot add tags to it.')
        desc = json.loads(tag_meta.description)
        tag_dict = { tag_meta.tag_name : {'record': record, 'label': desc['node_fullpath']}}
        print(f'Adding tag {tag_meta.tag_name} to global tag {self._global_tag.name}')
        self._tag_list.append(tag_dict)
        return self._tag_list
    
    def get_global_tag(self) -> GlobalTagDto:
        """
        Get the global tag.

        Returns:
            GlobalTagDto: The global tag DTO.
        """
        return self._global_tag
    
    def get_mappings(self) -> list:
        """
        Get the tag mappings.

        Returns:
            list: The tag mappings.
        """
        return self._tag_list