"""test class CrestApiContainers"""
import json
import logging

import types
import os
import sys
from unittest import mock
import pytest
from requests import Response

# make sure the pycrest.api used is the local one (for coverage)
BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
sys.path.insert(0, BASE_DIR)
# pylint: disable=import-error
from pycrest.api.crest_cond_container import CrestCondContainer
from pycrest.api.crest_cond_tag_manager import TagManager
from pycrest.api.tag_info_container import TagInfoContainer
from pycrest.api.crest_cond_builder import CrestCondBuilder
from hep.crest.client.models import (
    TagMetaDto, TagDto, GlobalTagDto
)
from pycrest.api.crest_api import CrestApi

log = logging.getLogger("test_crestapi_containers")
logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s %(levelname)s [%(name)s] %(message)s",
)

@pytest.fixture(scope="module")
def crest_client():
    """return sync CrestApi"""
#    sync_client = CrestApi("http://localhost:8090/api")
    sync_client = CrestApi("http://crest-service:8080/api")
    yield sync_client

def test_create_tag(crest_client):
    """test Create Tag"""
    api = crest_client
    name = 'test_tag'
    description = 'a brand new test tag'
    time_type = 'time'
    try:
        print('Create a tag %s' % name)
        tag = TagDto(
            name=name,
            description=description,
            time_type=time_type,
            payload_spec='JSON',
            synchronization='none',
            last_validated_time=-1,
            end_of_validity=-1
        )
        # Create a new tag
        api_response = api.create_tag(tag)
        assert api_response.name == name

        # Find the tag
        api_response = api.find_tag(name=name)
        assert api_response.name == name

        # Delete the tag
        api_response = api.remove_tag(name=name)
        assert None == api_response

    except Exception as e:
        assert False
        
def test_create_global_tag(crest_client):
    """test Create Global Tag"""
    api = crest_client
    global_tag = 'test_global_tag'
    description = 'a brand new test global tag'
    try:
        # Create a new global tag
        globaltag = GlobalTagDto(
            name=global_tag,
            description=description,
            release='1.0',
            scenario='test',
            validity=0,
            workflow='all',
            type='T'
        )
        api_response = api.create_global_tag(globaltag, force='false')
        assert api_response.name == global_tag

        # Find the global tag
        api_response = api.find_global_tag(name=global_tag)
        assert api_response.name == global_tag

        # Delete the global tag
        api_response = api.remove_global_tag(name=global_tag)
        assert None == api_response
    except Exception as e:
        assert False

def test_bad_url():
    """test bad url"""
    with pytest.raises(Exception):
        api = CrestApi(host='http://bad_service:8090/api')
        api.list_tags(name='%')
