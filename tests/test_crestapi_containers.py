"""test class CrestApiContainers"""
import json
import logging

import types
import os
import sys
from unittest import mock
import pytest
from requests import Response

# make sure the pycrest.api used is the local one (for coverage)
BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
sys.path.insert(0, BASE_DIR)
# pylint: disable=import-error
from pycrest.api.crest_cond_container import CrestCondContainer
from pycrest.api.crest_cond_tag_manager import TagManager
from pycrest.api.tag_info_container import TagInfoContainer
from pycrest.api.crest_cond_builder import CrestCondBuilder
from hep.crest.client.models import (
    TagMetaDto, GlobalTagDto, TagDto, StoreDto, StoreSetDto, IovSetDto, IovDto
)
from pycrest.api.crest_api import CrestApi

log = logging.getLogger("test_crestapi_containers")
logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s %(levelname)s [%(name)s] %(message)s",
)

@pytest.fixture(scope="module")
def crest_client():
    """return sync CrestApi"""
    sync_client = CrestApi("fake://crest.cern.ch/")
    yield sync_client

def test_cond_container():
    """test CrestCondContainer"""
    p_spec = { 'TileCalibBlob': 'Blob64k' }

    container = CrestCondContainer(payload_spec=p_spec)
    rec = container.add_record(attribute_list={'TileCalibBlob': b'1234567890'})
    container.add_data(channel_id=1, record=rec)
    payload_streamer = {'author': 'myname'}
    container.add_iov(since=1000, data=container.get_payload(), streamer_info=payload_streamer)
    json_array=container.flush_iovs()
    print(json_array)
    storedto = StoreDto(
                since=int(1000),
                data='{"data": {"1": ["MTIzNDU2Nzg5MA=="]}}',
                streamer_info='{"author": "myname"}'
            )
    out_json = [storedto]
    assert len(json_array) == 1
    assert json_array[0] == out_json[0]

def test_tag_info_container():
    """test TagInfoContainer"""
    tag_info = TagInfoContainer(tagname='mytag', folder='myfolder', schema='myschema')
    tag_info.add_record(name='TileCalibBlob', type='Blob64k')
    tag_info.add_channel(channel_id='1', channel_name='mychannel')
    tag_info.set_node_description(node_description='mydescription')
    tag_info.add_channel(channel_id='2', channel_name='mychannel2')
    d = {'schema': tag_info.get_schema(), 'node_fullpath': tag_info.get_folder()}
    meta = {
        'description': json.dumps(d),
        'chansize': tag_info.get_channels_size(),
        'colsize': tag_info.get_columns_size(),
        'tag_info': json.dumps(tag_info.get_tag_info())
    }

    assert tag_info.get_channels_size() == 2
    assert tag_info.get_tag_info_params() == meta

def test_tag_manager():
    """test TagManager"""
    name = 'TEST-GT-01'
    globaltag = GlobalTagDto(
        name=name,
        description='mydescription',
        release='myrelease',
        scenario='myscenario',
        validity=0,
        workflow='myworkflow',
        type='T'
    )
    tag_manager = TagManager(globaltag)

    tag_info_container = TagInfoContainer('TEST-TAG-01', 'myfolder', 'myschema')
    tag_info_container.add_record(name='TileCalibBlob', type='Blob64k')
    tag_info_container.set_node_description(node_description='mydescription')
    tag_info_container.add_channel(channel_id='1', channel_name='mychannel')
    tag_info_container.add_channel(channel_id='2', channel_name='mychannel2')
    
    tm = TagMetaDto()
    tm.tag_name = 'TEST-TAG-01'
    tm.description = json.dumps(tag_info_container.get_description())
    tm.chansize = len(tag_info_container.get_channel_list())
    tm.colsize = tag_info_container.get_columns_size()
    tm.metaInfo = tag_info_container.get_tag_info_params()

    tag_manager.add_tag(tm, 'active')
    assert tag_manager.get_global_tag() == globaltag

    mappings = tag_manager.get_mappings()
    assert len(mappings) == 1
    assert mappings[0] == {'TEST-TAG-01': {'record': 'active', 'label': 'myfolder'}}

def test_tag_manager_locked():
    """test TagManager locked"""
    name = 'TEST-GT-01'
    globaltag = GlobalTagDto(
        name=name,
        description='mydescription',
        release='myrelease',
        scenario='myscenario',
        validity=0,
        workflow='myworkflow',
        type='L'
    )
    tag_manager = TagManager(globaltag)

    tag_info_container = TagInfoContainer('TEST-TAG-01', 'myfolder', 'myschema')
    tag_info_container.add_record(name='TileCalibBlob', type='Blob64k')
    tag_info_container.set_node_description(node_description='mydescription')
    tag_info_container.add_channel(channel_id='1', channel_name='mychannel')
    tag_info_container.add_channel(channel_id='2', channel_name='mychannel2')
    
    tm = TagMetaDto()
    tm.tag_name = 'TEST-TAG-01'
    tm.description = json.dumps(tag_info_container.get_description())
    tm.chansize = len(tag_info_container.get_channel_list())
    tm.colsize = tag_info_container.get_columns_size()
    tm.metaInfo = tag_info_container.get_tag_info_params()

    with pytest.raises(ValueError):
        tag_manager.add_tag(tm, 'active')
    assert tag_manager.get_global_tag() == globaltag

    mappings = tag_manager.get_mappings()
    assert len(mappings) == 0

def test_cond_builder():
    """test CrestCondBuilder"""
    builder = CrestCondBuilder(schema='myschema', folder='myfolder', tagname='TEST-TAG-01')
    builder.set_payload_spec({'TileCalibBlob': 'Blob64k'})
    builder.set_channel_dict({'1': 'mychannel', '2': 'mychannel2'})
    builder.set_node_description('mydescription')
    tag_info_container = builder.build_tag_info()

    d = {'schema': 'myschema', 'node_fullpath': 'myfolder'}
    meta = {
        'description': json.dumps(d),
        'chansize': tag_info_container.get_channels_size(),
        'colsize': tag_info_container.get_columns_size(),
        'tag_info': json.dumps(tag_info_container.get_tag_info())
    }

    assert tag_info_container.get_tag_name() == 'TEST-TAG-01'
    assert tag_info_container.get_folder() == 'myfolder'
    assert tag_info_container.get_schema() == 'myschema'
    assert tag_info_container.get_columns_size() == 1
    assert tag_info_container.get_channels_size() == 2
    assert tag_info_container.get_description() == {'schema': 'myschema', 'node_fullpath': 'myfolder'}
    assert tag_info_container.get_tag_info_params() == meta
