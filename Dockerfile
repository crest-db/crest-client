#_____________________________________________________________________________
#
# Andrea Formica - February 2024
#
# Recipe to build an image for the PyCrest package: use the pycrest-openapi image.
# _____________________________________________________________________________
#
# Optimized:
#      * Most RUN commands are grouped in a single one to minimize
#        the number of layers and allow for clean-up after compilation
# ___________________________________________________________________________

# start from minimal image
FROM registry.cern.ch/crest/pycrest-openapi:5.0

# set up workdir
COPY pycrest /home/pycrest
COPY setup.py /home/setup.py
COPY README.md /home/README.md
COPY requirements.txt /home/requirements.txt
COPY examples /home/examples
COPY scripts /home/scripts

WORKDIR /home/

RUN pip install --upgrade pip && pip3 install -r ./requirements.txt \
    && pip3 install -e . \
    # clean up
    && rm -rf ${HOME}/.cache/pip

CMD ["bash"]
