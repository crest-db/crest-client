# CLI basic scripts
We deliver a bash and a python script that are ment for quick tests on the server, more to get an idea of the functionalities or to perform some operation by the server administrators when testing data insertions and retrieval. 
The scripts are not providing full API capabilities and are conceived as examples.

For real fully fledged python client you should use the main package provided in this project.


## Table of Contents
1. [BASH client](#bash-client)
2. [Python client](#python-client)
3. [List of functions](#list-of-functions)
4. [Output selection](#output-selection)

## BASH client
The bash client can be used inside CERN. 
To get an help just type:
```
./crest-cli.sh
```
To define the server URL use (e.g.)
```
export CREST_SERVER_PATH="http://crest.cern.ch/api-v4.0"
```
## Python client
The script has been tested to work with version `3.11` of python.
The python client can be used anywhere, provided you have a `socks` proxy available (if you are outside GPN).
In order to select the server and the proxy use the `--host` and `--proxy` arguments:
```
python crest_cli.py --host http://crest.cern.ch/api-v4.0 --proxy socks5://localhost:3129
```
For the socks proxy argument we did assume that you have an ssh connection to CERN like this:
```
ssh -D3129 lxplus.cern.ch
```

## List of functions
Below we show the list of functions in both clients.

* *create_tag* [name] [description]: create a tag
```
  Example: ./crest_cli.sh create_tag "TEST-ITK-01" "Some test tag for ITK"
```  
* *create_gtag* [name] [description]: create a global tag
```
  Example: ./crest_cli.sh create_gtag "TEST-GT-01" "Some test global tag"
```
* *link_tag2gtag* [tag_name] [gtag_name] [record] [label]: associate a tag to a global tag
```
  Example: ./crest_cli.sh link_tag2gtag "TEST-ITK-01" "TEST-GT-01" "none" "/ITK/SOMECAL"
```
* *store_data* [tag_name] [since] [filepath] [filename]: add a file with a since time to the given tag
```
  Example: ./crest_cli.sh store_data "TEST-ITK-01" 1000 "/my/file/path" "my_cond_filename"
```
* *trace_tags* [globaltag_name]: list tags associated to the global tag
```
  Example: ./crest_cli.sh trace_tags "TEST-GT-01" 
```
* *list_iovs* [tag_name]: list the iovs in a tag
```
  Example: ./crest_cli.sh list_iovs "TEST-ITK-01" 
```
* *get_data* [hash]: get the payload data using the hash
```
  Example: ./crest_cli.sh get_data "somehashfromtheiovlist" 
```
* *get_data_info* [hash]: get the payload data info using the hash
```
  Example: ./crest_cli.sh get_data_info "somehashfromtheiovlist" 
```
* *get_monitoring_info* [tag_pattern]: get an overview of tags content
```
  Example: ./crest_cli.sh get_monitoring_info "LAR%25" 
```

## Output selection
Some additional parsing of the output content can be provided by the `jq` tool (available on lxplus).
We illustrate below some possible actions.

### Pretty print with jq
You need to select only the JSON response. To do that we use `egrep` and `awk`:
```
./scripts/crest_cli.sh list_tags "TEST%25" | egrep "Output" | awk -F 'Output:' '{print $2}' | jq
```
Suppose now you want only to extract the `name` field from the list of retrieved tags:
```
./scripts/crest_cli.sh list_tags "TEST%25" | egrep "Output" | awk -F 'Output:' '{print $2}' | jq -r '.resources[].name'
```
To get monitoring information and dump only the total volume of a tag:
```
./crest_cli.sh get_monitoring_info LAR%25 | egrep Output | awk -F 'Output:' '{print $2}' | jq -r '.resources[] | {tag: .tagname, total: .totvolume}'
```