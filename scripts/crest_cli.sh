#!/bin/bash

## Define the HOST : this is the CREST server path
host=${CREST_SERVER_PATH}
# Below we provide some examples for the environment variables
## CREST_SERVER_PATH="http://crest.cern.ch/api-v4.0"
## SOCKS_PROXY="-x socks5://localhost:3129"

generate_tag_data()
{
  tag_name=$1
  tag_description=$2
  cat <<EOF
{
  "description": "${tag_description}",
   "endOfValidity": -1.0,
   "lastValidatedTime": -1.0,
   "name": "${tag_name}",
   "payloadSpec": "JSON",
   "synchronization": "any",
   "timeType": "time"
}
EOF
}

generate_gtag_data()
{
  gtag_name=$1
  gtag_description=$2
  cat <<EOF
{
  "description": "${gtag_description}",
  "validity": -1.0,
  "name": "${gtag_name}",
  "release": "v1",
  "scenario": "undefined",
  "workflow": "undefined",
  "type": "U"
}
EOF
}

generate_link_data()
{
  gtag_name=$1
  tag_name=$2
  record=$3
  label=$4
  cat <<EOF
{
  "globalTagName": "${gtag_name}",
  "tagName": "${tag_name}",
  "record": "${record}",
  "label": "${label}"
}
EOF
}

function get_file_string() {
  local fpath=$1
  local filename=$2
  local pstr
    # Check if the file is binary
  if [[ -n $(file -b --mime-encoding "${fpath}/${filename}" | grep -i "binary") ]]; then
    pstr=$(base64 < "${fpath}/${filename}")
  else
    pstr=$(cat "${fpath}/${filename}")
  fi
  echo $pstr
}

generate_payload_data()
{
  tag_name=$1
  since=$2
  fpath=$3
  filename=$4
  pyld_data=$(get_file_string "$fpath" "$filename")
  cat <<EOF
{
  "size": 1,
  "datatype": "iovs",
  "format": "StoreSetDto",
  "page": null,
  "filter": null,
  "resources":[
  { "since" : $since, "data": "${pyld_data}", "streamerInfo": "{\"filename\": \"${filename}\"}"}
  ]
};type=application/json
EOF
}

# Function to create a tag
create_tag() {
  local name="$1"
  local description="$2"
  echo "Creating tag with name: $name and description: $description"
  # Add your code to create a tag here
  post_data=$(generate_tag_data "$name" "$description")
  echo "Use data $post_data"
  resp=`curl ${SOCKS_PROXY} -X POST -H "Accept: application/json" -H "Content-Type: application/json" "${host}/tags" --data "${post_data}"`
  echo "Output: " $resp
}

# Function to create a global tag
create_gtag() {
  local name="$1"
  local description="$2"
  echo "Creating global tag with name: $name and description: $description"
  post_data=$(generate_gtag_data "$name" "$description")
  # Add your code to create a global tag here
  echo "Use data $post_data"
  resp=`curl ${SOCKS_PROXY} -X POST -H "Accept: application/json" -H "Content-Type: application/json" "${host}/globaltags" --data "${post_data}"`
  echo "Output: " $resp
}

# Function to link a tag to a global tag
link_tag2gtag() {
  local tag_name="$1"
  local gtag_name="$2"
  local record="$3"
  local label="$4"
  echo "Linking tag '$tag_name' to global tag '$gtag_name'"
  # Add your code to link the tag to the global tag here
  post_data=$(generate_link_data "$gtag_name" "$tag_name" "$record" "$label")
  echo "Use data $post_data"
  resp=`curl ${SOCKS_PROXY} -X POST -H "Accept: application/json" -H "Content-Type: application/json" "${host}/globaltagmaps" --data "${post_data}"`
  echo "Output: " $resp
}

# Function to store data
store_data() {
  local name="$1"
  local since="$2"
  local path="$3"
  local filename="$4"
  echo "Storing data with name: $name, since: $since, and filename: $filename"
  # Add your code to store data here
  post_data=$(generate_payload_data "$name" "$since" "$path" "$filename")
  echo "Use data $post_data"
  echo $post_data > ${name}_${since}_store.json
  resp=`curl ${SOCKS_PROXY} -X PUT -H "Accept: application/json" -H "Content-Type: multipart/form-data" --form tag=$name --form endtime=0 --form version="1.0" --form storeset=@${name}_${since}_store.json --form objectType="JSON" "${host}/payloads"`
  echo "Output: " $resp
}


# Function to trace tags using a global tag name
trace_tags() {
  local name="$1"
  echo "Trace tags in global tag: $name"
  # Add your code to store data here
  resp=`curl ${SOCKS_PROXY} -X GET -H "Accept: application/json" -H "Content-Type: application/json" "${host}/globaltagmaps/${name}"`
  echo "Output: " $resp
}

list_iovs() {
  local name="$1"
  echo "List IOVs in tag: $name"
  # Add your code to store data here
  resp=`curl ${SOCKS_PROXY} -X GET -H "Accept: application/json" -H "Content-Type: application/json" "${host}/iovs?tagname=${name}"`
  echo "Output: " $resp
}

list_tags() {
  local name="$1"
  echo "List tags: $name"
  # Add your code to get data here
  resp=`curl ${SOCKS_PROXY} -X GET -H "Accept: application/json" -H "Content-Type: application/json" "${host}/tags?name=${name}"`
  echo "Output: " $resp
}

list_globaltags() {
  local name="$1"
  echo "List globaltags: $name"
  # Add your code to get data here
  resp=`curl ${SOCKS_PROXY} -X GET -H "Accept: application/json" -H "Content-Type: application/json" "${host}/globaltags?name=${name}"`
  echo "Output: " $resp
}

get_data() {
  local hash="$1"
  echo "Get data with hash: $hash"
  # Add your code to store data here
  resp=`curl ${SOCKS_PROXY} -X GET "${host}/payloads/${hash}"`
  echo "Output: " $resp
}

get_data_info() {
  local hash="$1"
  echo "Get data info with hash: $hash"
  # Add your code to store data here
  resp=`curl ${SOCKS_PROXY} -X GET "${host}/payloads?hash=${hash}"`
  echo "Output: " $resp
}

get_monitoring_info() {
  local tagname="$1"
  echo "Get monitoring info with tag pattern: $tagname"
  # Add your code to get data here
  resp=`curl ${SOCKS_PROXY} -X GET "${host}/monitoring/payloads?tagname=${tagname}"`
  echo "Output: " $resp
}

# Main script
if [ "$#" -lt 1 ]; then
  echo "Usage: $0 [function] [arguments]"
  echo "Available functions:"
  echo "  create_tag [name] [description]"
  echo "  create_gtag [name] [description]"
  echo "  link_tag2gtag [tag_name] [gtag_name] [record] [label]"
  echo "  store_data [tag_name] [since] [filepath] [filename]"
  echo "  trace_tags [globaltag_name]"
  echo "  list_globaltags [gtag_name_pattern]"
  echo "  list_tags [tag_name_pattern] (use %25 as wildcard))"
  echo "  list_iovs [tag_name]"
  echo "  get_data [hash]"
  echo "  get_data_info [hash]"
  echo "  get_monitoring_info [tag_name_pattern] (use %25 as wildcard)"
  echo " Server selection: use the CREST_SERVER_PATH environment variable to set the server path"
  echo " SOCKS proxy: use the SOCKS_PROXY environment variable to set the proxy"
  exit 1
fi

function_name="$1"
shift # Remove the function name from the argument list

case "$function_name" in
  "create_tag")
    create_tag "$@"
    ;;
  "create_gtag")
    create_gtag "$@"
    ;;
  "link_tag2gtag")
    link_tag2gtag "$@"
    ;;
  "store_data")
    store_data "$@"
    ;;
  "trace_tags")
    trace_tags "$@"
    ;;
  "list_iovs")
    list_iovs "$@"
    ;;
  "list_tags")
    list_tags "$@"
    ;;
  "list_globaltags")
    list_globaltags "$@"
    ;;
  "get_data")
    get_data "$@"
    ;;
  "get_data_info")
    get_data_info "$@"
    ;;
  "get_monitoring_info")
    get_monitoring_info "$@"
    ;;
  *)
    echo "Invalid function: $function_name"
    exit 1
    ;;
esac
