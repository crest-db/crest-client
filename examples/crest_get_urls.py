import re
import aiohttp
import asyncio
import logging

# Function to extract URLs from the given file
def extract_urls(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    urls = []
    for line in lines:
        match = re.search(r'http[s]?://[^\s]+', line)
        if match:
            urls.append(match.group(0))
    
    return urls

# Async function to fetch the URL and log if status is not 200
async def fetch(session, url, logger):
    logger.info(f"Fetching URL: {url}")
    async with session.get(url) as response:
        status = response.status
        text = await response.text()
        if status != 200:
            logger.warning(f"URL: {url} returned status code {status}")
        logger.info(f"URL: {url}\nResponse: {text[:50]}...\n")
        return text

# Async function to fetch all URLs
async def fetch_all(urls, logger):
    async with aiohttp.ClientSession() as session:
        tasks = []
        for url in urls:
            tasks.append(fetch(session, url, logger))
        responses = await asyncio.gather(*tasks)
        return responses

# Main function to extract URLs, set up logging, and run the async fetch
def main(file_path):
    # Set up logging
    logging.basicConfig(filename='crest_get_log.txt', level=logging.INFO, 
                        format='%(asctime)s - %(levelname)s - %(message)s')
    logger = logging.getLogger()

    # Extract URLs from the file
    urls = extract_urls(file_path)
    
    # Run the async fetching
    responses = asyncio.run(fetch_all(urls, logger))
    
    # Optionally print responses or handle them as needed
    # for response in responses:
    #     print(response)

if __name__ == "__main__":
    main('urls.txt')
