import random as rndm
import json
import time
from pycrest.api.crest_api import CrestApi

# Socks proxy
# create an instance of the API class
def socks(proxy_host):
    SOCKS5_PROXY_HOST = proxy_host
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

socks('localhost')


# create an instance of the API class
api_instance = CrestApi(host='http://crest.cern.ch/api-v4.0')
# tag name
name = 'test_lumi_01'
# tag description
description = 'Simulated fake lumi data every N minutes'
# tag time type
time_type = 'run-lumi'

print('Create a tag %s' % name)
try:
    # Create a new tag
    api_response = api_instance.create_tag(name=name, timeType=time_type, description=description)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->create_tag: %s\n" % e)

#  Add metadata to the tag
tag_info = {'channel_list':[{'0':'ATLAS_PREFERRED'}],
            'node_description':'<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\"/></addrHeader><typeName>CondAttrListCollection</typeName>',
            'payload_spec':'AlgorithmID:UInt32,LBAvInstLumi:Float,LBAvEvtsPerBX:Float,LumiType:UInt32,Valid:UInt32,BunchInstLumi:Blob64k'}

api_instance.create_tag_meta(name=name, tag_info=json.dumps(tag_info), chansize=1, colsize=6)

lumi_counter = 0
run_number = 2000
while True:
    time.sleep(10)
    lumi_counter += 1
    xlumi = run_number << 32 | lumi_counter
    if (lumi_counter > 999):
        break
    # Insert data into the tag
    algoId = rndm.randint(0, 1000)
    instLumi = rndm.random() * 1000
    evtsPerBX = rndm.random() * 1000
    lumiType = rndm.randint(0, 1000)
    valid = rndm.randint(0, 1)
    bunchInstLumi = rndm.random() * 1000

    sinfo = {'run-lumi': f'{run_number}-{lumi_counter}'}

    lumi_pyld = {
        '0': [algoId, instLumi, evtsPerBX, lumiType, valid, bunchInstLumi] 
    }
    
    resources = [
        {'since': float(xlumi), 'data': json.dumps(lumi_pyld), 'streamerInfo': json.dumps(sinfo)}
    ]
    print(f'Store data {resources} in tag {name} at time {xlumi} with lumi counter = {lumi_counter}')
    try:
        api_response = api_instance.store_data(tagname=name, json_array=resources)
        print(api_response)
    except Exception as e:
        print("Exception when calling CrestApi->store_json_data: %s\n" % e)

