from pycrest.api.crest_api import CrestApi
from hep.crest.client.models import (
    IovSetDto, HTTPResponse, TagMetaSetDto, TagMetaDto, TagSetDto, TagDto, GlobalTagDto, 
    GlobalTagSetDto, GlobalTagMapDto, StoreSetDto, StoreDto, RunLumiInfoDto, RunLumiSetDto
)
# create an instance of the API class
def socks(proxy_host):
    SOCKS5_PROXY_HOST = proxy_host
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

socks('localhost')
api_instance = CrestApi(host='http://crest-undertow-api.web.cern.ch/api-v4.0')

# tag name
name = 'test_tag'
# tag description
description = 'a brand new test tag'
# tag time type
time_type = 'time'

params = {
    'payload_spec': 'ascii',
    'description': description,
    'synchronization': 'none',
    'last_validated_time': -1.,
    'end_of_validity': -1.,
}
tagdto = TagDto(
    name=name,
    description=params['description'],
    time_type=time_type,
    payload_spec=params['payload_spec'],
    synchronization=params['synchronization'],
    last_validated_time=params['last_validated_time'],
    end_of_validity=params['end_of_validity']
)

print('Create a tag %s' % name)
try:
    # activate socks
    # Create a new tag
    api_response = api_instance.create_tag(tagdto)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->create_tag: %s\n" % e)

print('Find a tag %s' % name)
try:
    # activate socks
    # Create a new tag
    api_response = api_instance.find_tag(name=name)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->create_tag: %s\n" % e)

# Remove the tag
print('Delete tag %s' % name)
try:
    api_response = api_instance.remove_tag(name=name)
    print('Done')
except Exception as e:
    print("Exception when calling CrestApi->remove_tag: %s\n" % e)
