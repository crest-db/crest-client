#!/bin/bash
fname=$1
# Base64 encoded string
encoded_str=$(cat ${fname})

# Decode the base64 string and extract the first byte
first_byte=$(echo "$encoded_str" | base64 --decode | head -c 1)

# Print the first byte in decimal format
printf "%d\n" "'$first_byte"


# Convert to hexadecimal
hex_representation=$(echo -n "$first_byte" | xxd -p)

# Convert to binary
binary_representation=$(echo "obase=2; ibase=16; $(echo "$hex_representation" | tr 'a-f' 'A-F')" | bc)

echo "First byte (binary): $(printf "%08d" "$binary_representation")"
echo "First byte (hexadecimal): $hex_representation"
