from pycrest.api.crest_api import CrestApi
from pycrest.api.crest_cond_builder import CrestCondBuilder
from pycrest.api.crest_cond_container import CrestCondContainer
from pycrest.api.tag_info_container import TagInfoContainer
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.tag_meta_dto import TagMetaDto
import json
import time
# Author: Andrea Formica (CERN)
# Example taken from LAR COOL data, using folder /LAR/BadChannelsOfl/BadChannels.
#
# In this snippet we will create a new tag, add tag info and add iovs to the tag.
# The tag will be created with a specific payload specification and a set of channels
# The iovs will be created with a random payload for each channel, and the since will be incremented by 100.
# The data are stored in one go at the end of the process, for all the iovs.
# This avoid the need to call crest api http requests for each iov.
#
# To read the data back use the script crest-read-lar.py
# Every time you run this script you will create a new tag with new data, and remove the old one.



# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-j23.cern.ch:8080/api-v5.0')
api_instance = CrestApi(host=host)
socks = os.getenv('CREST_SOCKS', 'False')
if socks == 'True':
    api_instance.socks()

# Define tag
desttag='Test-LARBadChannelsOflBadChannels-RUN2-00'
# tag description
description = 'A Test LAR CREST tag'
# tag time type
time_type = 'time'

def random_data():
    import random
    import string
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=4096))

def random_record():
    import random
    return {
        'ChannelSize': random.randint(0, 100),
        'StatusWordSize': random.randint(0, 100),
        'Endianness': random.randint(0, 1),
        'Version': random.randint(0, 100),
        'Blob': random_data()
    }

# Create a payload record
# use the columns defined before to create a typical payload: we use a Cool compatible format
# In this function we emulate new data for different run-lumi and channels
def generate_since_data(since=0, crest_payload=None):
    payload_streamer = {'author': 'larexpert', 'comment': 'this has new Blob data'}
    for chan in range(0, 8):
        rec = random_record()
        pdata = crest_payload.add_record(attribute_list=rec)
        crest_payload.add_data(channel_id=chan, record=pdata)
    crest_payload.add_iov(since=since, data=crest_payload.get_payload(), streamer_info=payload_streamer)
    print(f'iov list is: {crest_payload.get_iov_list_size()}')
    return crest_payload

# Create an helper class for your "specific" container (which corresponds to a COOL folder+tag)
class LarCondBadChannel(CrestCondBuilder):
    p_spec = { 'ChannelSize': 'UInt32', 'StatusWordSize': 'UInt32', 'Endianness': 'UInt32', 'Version': 'UInt32', 'Blob': 'Blob64k' }
    c_dict = { '1': 'LAR-Bad-01', '2': 'LAR-Bad-02', '3': 'LAR-Bad-03', '4': 'LAR-Bad-04', '5': 'LAR-Bad-05', '6': 'LAR-Bad-06', '7': 'LAR-Bad-07', '8': 'LAR-Bad-08'}
    n_description = '<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\"/></addrHeader><typeName>CondAttrListCollection</typeName>'
    schema = 'COOLOFL_LAR'
    folder = '/LAR/BadChannelsOfl/BadChannels'
    tagname = desttag

    def __init__(self):
        """
        Constructor
        Use the schema, folder and tagname to create a new tag meta info.
        Use the properties then to finalize the tag meta creation.
        """
        CrestCondBuilder.__init__(self, schema=self.schema, folder=self.folder, tagname=self.tagname)
        CrestCondBuilder.init_container(self, p_spec=self.p_spec, n_description=self.n_description, c_dict=self.c_dict)


#  Create a specific container for LAR Bad Channels
larcond = LarCondBadChannel()

# Clean up the existing tag
# Remove the tag
print('Delete tag %s if exists' % desttag)
try:
    tag = api_instance.find_tag(name=desttag)
    if tag is not None and isinstance(tag, TagDto):
        tagname = tag.name
        description = tag.description
        print(f'Found tag {tagname} - {description} for name {desttag}: removing it...')
        api_response = api_instance.remove_tag(name=desttag)
except Exception as e:
    print("Exception when calling CrestApi: %s\n" % e)

# Start creating everything in Crest
try:
    start_time = time.time()
    # Create a new tag
    print(f'Creating tag {desttag}')
    m_tagdto = larcond.build_tag(tag_description='A Test LAR CREST tag', time_type='time')
    api_response = api_instance.create_tag(m_tagdto)
    if isinstance(api_response, TagDto):
        print(f'Created tag {api_response.name}')
    else:
        print(f'Error creating tag {desttag}: {api_response}')
    # Add tag info
    tag_info_dto = larcond.get_tag_info().get_tag_info_dto()

    print(f'Creating tag meta info for {desttag} with parameters: {tag_info_dto}')
    api_response = api_instance.create_tag_meta(tag_info_dto)
    if isinstance(api_response, TagMetaDto):
        print(f'Created tag meta info for {api_response.tag_name}')
    else:
        print(f'Error creating tag meta info for {desttag}: {api_response}')
    # Add iovs and payload
    larcond_payload = larcond.get_payload()
    for since in range(0, 10000, 100):
        larcond_payload = generate_since_data(since, larcond_payload)
        print(f'Add data for since {since}')
    # Store the data in one go
    store_set_dto = larcond_payload.get_store_set()
    api_response = api_instance.store_data(tag=desttag, store_set=store_set_dto)
    print(f'Stored data in tag {desttag}')
    end_time = time.time()
    # Calculate the elapsed time
    elapsed_time = end_time - start_time
    print("Time elapsed:", elapsed_time, "seconds")
except Exception as e:
    print("Exception when calling CrestApi : %s\n" % e)

