from pycrest.api.crest_api import CrestApi
from pycrest.api.tag_info_container import TagInfoContainer
from pycrest.api.crest_cond_builder import CrestCondBuilder
from pycrest.api.crest_cond_tag_manager import TagManager
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.tag_meta_dto import TagMetaDto
from hep.crest.client.model.global_tag_dto import GlobalTagDto
from hep.crest.client.model.global_tag_map_dto import GlobalTagMapDto
from hep.crest.client.model.global_tag_map_set_dto import GlobalTagMapSetDto
from hep.crest.client.model.tag_meta_set_dto import TagMetaSetDto
import json
import logging
import requests
import oracledb
import argparse
import datetime
# Author: Andrea Formica (CERN)
# Example for Global Tag Coordination.
#
# In this snippet we will create some tags with the tag info as a system expert would do.
# Then we will create a global tag and associate the tags to it.
# We are going to implement the LOCK and UNLOCK features in the CREST server later on.
# Example on how to run this command:
# python3 check_crest_tags.py --globaltag CREST-HLTP-2024-02 --password <the password for condtools>
#

# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-undertow-api.web.cern.ch/api-v4.0')
api_instance = CrestApi(host=host)
cool_max_validity = 9223372036854775807  # max validity for IOVs

# create an instance of the API class
def socks(proxy_host):
    SOCKS5_PROXY_HOST = proxy_host
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

#socks('localhost')

def get_global_tag_map(globaltag=None):

    try:
        api_response = api_instance.find_global_tag_map(name=globaltag)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->find_global_tag_map: %s\n" % e)

def get_tag_meta(tag=None):
    try:
        api_response = api_instance.find_tag_meta(name=tag)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->find_tag_meta: %s\n" % e)

def get_tag(tag=None):
    try:
        api_response = api_instance.find_tag(name=tag)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->find_tag: %s\n" % e)

def get_coolr_iovs(coolr_dic=None):
    url = 'http://atlas-coolr-api.web.cern.ch/api/iovs/tail?n=1&'
    url_with_params = url + 'db=' + coolr_dic['dbname'] + '&node=' + coolr_dic['node'] + '&schema=ATLAS_' + coolr_dic['schemaName']
    if 'tag_name' in coolr_dic:
        url_with_params = url_with_params + '&tag=' + coolr_dic['tag_name']
    try:
        api_response = requests.get(url_with_params)
        return api_response.json()
    except Exception as e:
        print("Exception when calling get_coolr_iovs: %s\n" % e)


def get_coolr_nodetag(coolr_dic=None):
    url = 'http://atlas-coolr-api.web.cern.ch/api'
    if 'tag_name' in coolr_dic:
        url_with_params = url + '/tags?tag=' + coolr_dic['tag_name'] + '&node=' + coolr_dic['node'] + '&db=' + coolr_dic['dbname'] + '&schema=ATLAS_' + coolr_dic['schemaName']
    else:
        url_with_params = url + '/nodes?node=' + coolr_dic['node'] + '&db=' + coolr_dic['dbname'] + '&schema=ATLAS_' + coolr_dic['schemaName']

    try:
        api_response = requests.get(url_with_params)
        return api_response.json()
    except Exception as e:
        print("Exception when calling get_coolr_nodetag: %s\n" % e)


def get_runinfo(run=None):
    url = f'http://atlas-run-info-api.web.cern.ch/api/runs?since={run}&until={run}&sort=runnumber:ASC'
    try:
        api_response = requests.get(url)
        return api_response.json()
    except Exception as e:
        print("Exception when calling get_runinfo: %s\n" % e)

def get_crest_iovs(name=None,since=None,until=None):
    try:
        api_response = api_instance.select_iovs(name=name, since=str(since), until=str(until),snapshot=0,sort='id.since:DESC',size=1)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->select_iovs: %s\n" % e)

def count_crest_iovs(name=None):
    try:
        api_response = api_instance.get_size(tagname=name)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->select_iovs: %s\n" % e)

# Argument parser setup to handle command-line arguments
def get_arguments():
    parser = argparse.ArgumentParser(description="CREST COOL IOV comparaison.")
    
    # Adding arguments
    parser.add_argument("--socks", required=False, default=False, help="Use socks proxy if True.")
    parser.add_argument("--password", required=True, help="Password for the Oracle DB.")
    parser.add_argument("--inputfile", required=True, help="A filename containing a json formatted list of schemas nodes tags.")
    parser.add_argument("--run", required=False, help="A run number for which we need conditions.")
   
    return parser.parse_args()

def load_intervals(password, schema, db, node, tag, precision, since, until):
    # Placeholder for loading intervals based on kwargs
    dsn_tns = "ATONR_ADG"  # Data Source Name or connection string (e.g., IP, port, and service name)
    username = "atlas_cond_tools_r"
    cursor = None
    connection = None

    try:
        oracledb.init_oracle_client()

        connection = oracledb.connect(user=username, password=password, dsn=dsn_tns)
        cursor = connection.cursor()

        # SQL Query using the configurable arguments
        query = f"""
        SELECT IOV_SINCE, NCHANS
        FROM TABLE(atlas_cond_tools.cool_select_pkg.f_Get_IovsSinceDistinct(
            'ATLAS_{schema}', 
            '{db}', 
            '{node}', 
            '{tag}', 
            {precision}, {since}, {until}))
        """
        print(f"Executing query: {query}")
        cursor.execute(query)
        rows = cursor.fetchall()

        # Array to hold SINCE and UNTIL values
        since_until_array = []

        # Iterate over rows and calculate UNTIL as the next IOV_SINCE
        for i in range(len(rows)):
            iov_since = rows[i][0]
            nchans = rows[i][1]

            # UNTIL is the next IOV_SINCE unless it's the last row
            if i < len(rows) - 1:
                until = rows[i + 1][0]
            else:
                until = None  # Set UNTIL to None for the last row

            # Add the (SINCE, UNTIL) tuple to the array
            since_until_array.append((iov_since, until))

            # Print current row with UNTIL
            # print(f"{iov_since:<15} {nchans:<7} {until}")
    
        return since_until_array
    
    except oracledb.Error as e:
        print(f"Error: {e}")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()
    pass

def main():
    # Set up logging
    logging.basicConfig(filename='crest_checktags_log.txt', level=logging.INFO, 
                        format='%(asctime)s - %(levelname)s - %(message)s')
    logger = logging.getLogger()
    args = get_arguments()
    socks = args.socks
    if socks == 'True':
        print('Using socks proxy....')
        api_instance.socks()

    if args.run:
        run = int(args.run)
        runinfo = get_runinfo(run=run)
        logger.info(f"Run info: {runinfo}")
        runstart = runinfo['resources'][0]['startat']
        duration = runinfo['resources'][0]['duration']
        # the format of runstart is '20230501T000000Z': YYYYMMDDTHHMMSSZ
        # convert to timestamp using format YYYYMMDD'T'HHMMSS
        runstart_timestamp = datetime.datetime.strptime(runstart, '%Y%m%dT%H%M%S').timestamp()
        runend_timestamp = runstart_timestamp + duration
        logger.info(f"Run start timestamp: {runstart_timestamp} + {duration} seconds = {runend_timestamp}")
    # We decide that we start from a json file with a list of schemas, dbs, nodes and tags
    inputfile = args.inputfile
    with open(inputfile) as f:
        mappings = json.load(f)
    #
    node_tag_list = mappings['resources']
    coolr_dic = {}
    for atag in node_tag_list:
        # Sequence of actions
        # Get cool tag meta and create the corresponding crest json dtos on filesystem.
        # Get the iovs from cool (using oracle query)
        #    If run is provided, limit the iovs to the run range
        # For each iov range dump the cooldump command to query data and dump it to a file.
        # 
        coolr_dic['schemaName'] = atag['schema']
        coolr_dic['dbname'] = atag['db']
        coolr_dic['node'] = atag['node']
        coolr_dic['tag_name'] = atag['tag']

        # Get the tag / node information from COOL
        cool_tag_info = get_coolr_nodetag(coolr_dic=coolr_dic)
        logger.info(f'Cool tag info: {cool_tag_info}')
        # Get the last IOV from COOL
        list_intervals = []
        if args.run:
            list_intervals = load_intervals(args.password, coolr_dic['schemaName'], coolr_dic['dbname'], coolr_dic['node'], coolr_dic['tag_name'], 0, runstart_timestamp, runend_timestamp)
            logger.info(f'List of intervals: {list_intervals}')
        else:
            list_intervals = load_intervals(args.password, coolr_dic['schemaName'], coolr_dic['dbname'], coolr_dic['node'], coolr_dic['tag_name'], 0, 0, 9223372036854775807)
        logger.info(f'List of intervals: {list_intervals}')

if __name__ == "__main__":
    main()
