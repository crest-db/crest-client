# Compare COOL and CREST
The examples here are ment to be used by experts to get friendly with differences between COOL and CREST.

## Installation
You need to install the packages `pycrest` and the `crest-client` in your environment.
This is already documented in the `README.md` at the root level of this package.

## Examples: COOL
To run COOL example do the following:
```shell
setupATLAS
asetup Athena,main,latest
```
Then you should remove the output file of the example if it is already present (`simplecool.db`).
In the examples we use a file `test.txt` to load some random data (interpreted as a string).
Create such a file if not present.
Use the command:
```shell
python cool-command.py
```
It will produce a sqlite file with a folder, tag, and an identical iov for 2 channels.

## Examples: CREST
To run CREST example you need to previously install the client libraries as mentioned above.
We rely on the presence of the `test.txt` file as in the previous example.
Then run the command:
```shell
python crest-store.py
```
The script connects to a CREST server and store the data directly in Oracle.
When you re-run the script the tag that has been stored previously is removed.
In order to check the data you have been reading you can use:
```shell
python crest-read.py
```

# Example inspired from specific systems
This section is under development. We intend to proceed with some system experts in order to provide instructions useful to them.
## Tile
The scripts `crest-store-tile.py` and `crest-read-tile.py` contains example which are taken from present TILE folder in COOL.

## LAR
The scripts `crest-store-lar.py` and `crest-read-lar.py` contains example which are taken from present LAR folder in COOL.

## Global tag coordination
The script `crest-manage-tag-gtag.py` contains an example to create a global_tag, some tags, and then associate the global_tag to the tags.
The script also remove everything already existing if it finds them in the CREST DB.