from pycrest.api.crest_api import CrestApi
from pycrest.api.crest_cond_container import CrestCondContainer
from pycrest.api.tag_info_container import TagInfoContainer
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.tag_meta_dto import TagMetaDto
import json

# Author: Andrea Formica (CERN)
# Example taken from LAR COOL data, using folder /LAR/BadChannelsOfl/BadChannels.
#
# In this snippet we will read a crest tag create by the script crest-store-lar.py
#
# Ths script checks the tag, list the iovs and get the payload for a given since.
#

# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-undertow-api.web.cern.ch/api-v4.0')
api_instance = CrestApi(host=host)
socks = os.getenv('CREST_SOCKS', 'False')
if socks == 'True':
    api_instance.socks()

# Define tag
desttag='Test-LARBadChannelsOflBadChannels-RUN2-00'

# Verify your data
# Check tag
print('Check tag %s' % desttag)
try:
    tag = api_instance.get_tag(name=desttag)
    if tag is not None and isinstance(tag, TagDto):
        tagname = tag.name
        description = tag.description
        print(f'Found tag {tagname} - {description} for name {desttag}')
except Exception as e:
    print("Exception when calling CrestApi: %s\n" % e)

# Query data from the tag
print('List iovs for tag %s' % desttag)
iovs = []
try:
    api_response = api_instance.find_all_iovs(tagname=desttag)
    if isinstance(api_response, IovSetDto):
        print(f'Found iovs list for tag {desttag} of size {api_response.size}')
        iovs = api_response.resources
        print(f'Found iovs : {iovs}')
# Get data for a given since
    since = 2020
    print(f'Get data for since {since}')
    sel_hash = api_instance.find_payload_hash(iovs, since)
    print(f'Select hash {sel_hash} for since {since}')
    api_response = api_instance.get_payload(hash=sel_hash)
    print(api_response)
    
    for iov in iovs:
        print(f'Found iov : {iov.since} - {iov.payload_hash}')
        # get payload for this since
        api_response = api_instance.get_payload(hash=iov['payload_hash'])
        print(f'Data : {api_response}')
        
except Exception as e:
    print("Exception when calling CrestApi for iov load or payload access: %s\n" % e)
