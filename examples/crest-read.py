from pycrest.api.crest_api import CrestApi
from pycrest.api.crest_cond_container import CrestCondContainer
from pycrest.api.tag_info_container import TagInfoContainer
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.tag_meta_dto import TagMetaDto
import json


# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-undertow-api.web.cern.ch/api-v4.0')
api_instance = CrestApi(host=host)
socks = os.getenv('CREST_SOCKS', 'False')
if socks == 'True':
    api_instance.socks()

# Define tag
desttag='TestCrest-REL-01'

# Verify your data
# Check tag
print('Check tag %s' % desttag)
try:
    tag = api_instance.find_tag(name=desttag)
    if tag is not None and isinstance(tag, TagDto):
        tagname = tag.name
        description = tag.description
        print(f'Found tag {tagname} - {description} for name {desttag}')
except Exception as e:
    print("Exception when calling CrestApi: %s\n" % e)

# Query data from the tag
print('List iovs for tag %s' % desttag)
iovs = []
try:
    api_response = api_instance.find_all_iovs(tagname=desttag)
    if isinstance(api_response, IovSetDto):
        print(f'Found iovs list for tag {desttag} of size {api_response.size}')
        iovs = api_response.resources
        for iov in iovs:
            print(f'Found iov : {iov.since} - {iov.payload_hash}')
            # get payload for this since
            api_response = api_instance.get_payload(hash=iov['payload_hash'])
            print(f'Data : {api_response}')
except Exception as e:
    print("Exception when calling CrestApi for iov load and payload access: %s\n" % e)
