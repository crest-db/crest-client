from pycrest.api.crest_api import CrestApi
from pycrest.api.tag_info_container import TagInfoContainer
from pycrest.api.crest_cond_builder import CrestCondBuilder
from pycrest.api.crest_cond_tag_manager import TagManager
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.tag_meta_dto import TagMetaDto
from hep.crest.client.model.global_tag_dto import GlobalTagDto
from hep.crest.client.model.global_tag_map_dto import GlobalTagMapDto
from hep.crest.client.model.global_tag_map_set_dto import GlobalTagMapSetDto
from hep.crest.client.model.tag_meta_set_dto import TagMetaSetDto
import json
import logging
import requests
import oracledb
import argparse
import datetime
# Author: Andrea Formica (CERN)
# Example for Global Tag Coordination.
#
# In this snippet we will create some tags with the tag info as a system expert would do.
# Then we will create a global tag and associate the tags to it.
# We are going to implement the LOCK and UNLOCK features in the CREST server later on.
# Example on how to run this command:
# python3 check_crest_tags.py --globaltag CREST-HLTP-2024-02 --password <the password for condtools>
#

# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-undertow-api.web.cern.ch/api-v4.0')
api_instance = CrestApi(host=host)
cool_max_validity = 9223372036854775807  # max validity for IOVs
one_hour = 3600  # seconds
# create an instance of the API class
def socks(proxy_host):
    SOCKS5_PROXY_HOST = proxy_host
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

#socks('localhost')

def get_global_tag_map(globaltag=None):

    try:
        api_response = api_instance.find_global_tag_map(name=globaltag)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->find_global_tag_map: %s\n" % e)

def get_tag_meta(tag=None):

    try:
        api_response = api_instance.find_tag_meta(name=tag)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->find_tag_meta: %s\n" % e)

def get_tag(tag=None):

    try:
        api_response = api_instance.find_tag(name=tag)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->find_tag: %s\n" % e)

def get_coolr_iovs(coolr_dic=None):
    url = 'http://atlas-coolr-api.web.cern.ch/api/iovs/tail?n=1&'
    url_with_params = url + 'db=' + coolr_dic['dbname'] + '&node=' + coolr_dic['node'] + '&schema=ATLAS_' + coolr_dic['schemaName']
    if 'tag_name' in coolr_dic:
        url_with_params = url_with_params + '&tag=' + coolr_dic['tag_name']
    try:
        api_response = requests.get(url_with_params)
        return api_response.json()
    except Exception as e:
        print("Exception when calling get_coolr_iovs: %s\n" % e)

def get_runinfo(run=None):
    url = f'http://atlas-run-info-api.web.cern.ch/api/runs?since={run}&until={run}&sort=runnumber:ASC'
    try:
        api_response = requests.get(url)
        return api_response.json()
    except Exception as e:
        print("Exception when calling get_runinfo: %s\n" % e)

def get_crest_iovs(name=None,since=None,until=None):
    try:
        if since is None:
            since = 0
        if until is None:
            until = cool_max_validity
        print(f"CREST: getting IOVs for {name} since {since} until {until}")
        api_response = api_instance.select_iovs(name=name, since=str(since), until=str(until),snapshot=0,sort='id.since:DESC',size=1,x_crest_query='AT')
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->select_iovs: %s\n" % e)

def count_crest_iovs(name=None):
    try:
        api_response = api_instance.get_size(tagname=name)
        return api_response
    except Exception as e:
        print("Exception when calling CrestApi->select_iovs: %s\n" % e)

# Argument parser setup to handle command-line arguments
def get_arguments():
    parser = argparse.ArgumentParser(description="CREST COOL IOV comparaison.")
    
    # Adding arguments
    parser.add_argument("--socks", required=False, default=False, help="Use socks proxy if True.")
    parser.add_argument("--password", required=True, help="Password for the Oracle DB.")
    parser.add_argument("--globaltag", required=True, help="Global tag for checks.")
    parser.add_argument("--run", required=False, help="A run number for which we need conditions.")
   
    return parser.parse_args()

def init_oracle_connection(password=None):
    dsn_tns = "ATONR_ADG"  # Data Source Name or connection string (e.g., IP, port, and service name)
    username = "atlas_cond_tools_r"
    try:
        oracledb.init_oracle_client()
        oracle_connection = oracledb.connect(user=username, password=password, dsn=dsn_tns)
        return oracle_connection
    except oracledb.Error as e:
        print(f"Error: {e}")
        return None
    
def load_intervals(oracle_connection, schema, db, node, tag, precision, since, until):
    # Placeholder for loading intervals based on kwargs
    cursor = None
    try:
        cursor = oracle_connection.cursor()
        # SQL Query using the configurable arguments
        query = f"""
        SELECT IOV_SINCE, NCHANS
        FROM TABLE(atlas_cond_tools.cool_select_pkg.f_Get_IovsSinceDistinct(
            'ATLAS_{schema}', 
            '{db}', 
            '{node}', 
            '{tag}', 
            {precision}, {since}, {until}))
        """
        print(f"Executing query: {query}")
        cursor.execute(query)
        rows = cursor.fetchall()

        # Array to hold SINCE and UNTIL values
        since_until_array = []

        # Iterate over rows and calculate UNTIL as the next IOV_SINCE
        for i in range(len(rows)):
            iov_since = rows[i][0]
            nchans = rows[i][1]

            # UNTIL is the next IOV_SINCE unless it's the last row
            if i < len(rows) - 1:
                until = rows[i + 1][0]
            else:
                until = None  # Set UNTIL to None for the last row

            # Add the (SINCE, UNTIL) tuple to the array
            since_until_array.append((iov_since, until))

            # Print current row with UNTIL
            # print(f"{iov_since:<15} {nchans:<7} {until}")
    
        return since_until_array
    
    except oracledb.Error as e:
        print(f"Error: {e}")
    finally:
        if cursor:
            cursor.close()
    pass

def filter_by_range(tuples_list, range_start, range_end):
    # Function to check if a tuple is inside the provided range
    def is_inside_range(tup, start, end):
        # Handle None as an open-ended range
        return (tup[1] is None or tup[1] > start) and tup[0] < end

    # Filter the list by the range
    filtered_list = [tup for tup in tuples_list if is_inside_range(tup, range_start, range_end)]
    
    return filtered_list

def extract_folder_description(tagmeta):
    coolr_dic = {}
    desc = tagmeta['resources'][0]['description']
    desc_dic = json.loads(desc)
    coolr_dic['dbname'] = desc_dic['dbname']
    coolr_dic['node'] = desc_dic['nodeFullpath']
    coolr_dic['schemaName'] = desc_dic['schemaName']    
    if 'HEAD' in tagmeta['resources'][0]['tag_name']:
        coolr_dic['tag_name'] = 'HEAD'
    else:
        coolr_dic['tag_name'] = tagmeta['resources'][0]['tag_name']
    return coolr_dic

def main():
    # Set up logging
    logging.basicConfig(filename='crest_checktags_log.txt', level=logging.INFO, 
                        format='%(asctime)s - %(levelname)s - %(message)s')
    logger = logging.getLogger()
    args = get_arguments()
    socks = args.socks
    if socks == 'True':
        print('Using socks proxy....')
        api_instance.socks()

    # Initialize Oracle connection
    pss = args.password
    logger.info(f'Initializing Oracle connection using {pss}')
    oracle_connection = init_oracle_connection(password=pss)
    if oracle_connection is None:
        logger.error("Failed to connect to Oracle DB.")
        return
    
    runstart_timestamp = None
    runend_timestamp = None
    if args.run:
        run = int(args.run)
        runinfo = get_runinfo(run=run)
        logger.info(f"Run info: {runinfo}")
        runstart = runinfo['resources'][0]['startat']
        duration = runinfo['resources'][0]['duration']
        # the format of runstart is '20230501T000000Z': YYYYMMDDTHHMMSSZ
        # convert to timestamp using format YYYYMMDD'T'HHMMSS
        runstart_timestamp = datetime.datetime.strptime(runstart, '%Y%m%dT%H%M%S').timestamp()
        runend_timestamp = runstart_timestamp + duration
        logger.info(f"Run start timestamp: {runstart_timestamp} + {duration} seconds = {runend_timestamp}")

    # Mandatory arguments: the global tag (which represents as well the destination for crest)
    globaltag = args.globaltag
    # Verify the global tag in CREST
    mappings = get_global_tag_map(globaltag=globaltag)
    tags = mappings['resources']
    coolr_dic = {}
    # Loop over the CREST tags and check COOL content
    for atag in tags:
        crest_tag = get_tag(tag=atag['tag_name'])
        tag_type = crest_tag['time_type']
        tag = atag['tag_name']
        tagmeta = get_tag_meta(tag=tag)
        if tagmeta['size'] > 1:
            logger.error(f"Tag {tag} has more than one meta")
        else:
            logger.info(f"Tag {tag} of IOV type {tag_type} has one meta")
            coolr_dic = extract_folder_description(tagmeta)
            logger.info(f"Tag {tag} corresponds to COOL {coolr_dic}")
            coolr_iovs = []
            last_cool_iov = None
            last_crest_iov = None
            # Get last iov in CREST
            crest_iovs = get_crest_iovs(name=tag,since=None,until=None)
            cool_tag = coolr_dic['tag_name']
            if 'HEAD' in coolr_dic['tag_name']:
                cool_tag = 'HEAD'

            if crest_iovs['size'] > 0:
                last_crest_iov = int(crest_iovs['resources'][0]['since'])
                logger.info(f"Tag {tag} has last IOV in CREST {last_crest_iov}")
            else:
                # Here we should dump a command for the migration of the corresponding cool tag
                logger.error(f"Tag {tag} has no IOVs in CREST")
                logger.error(f"Migration of COOL node/tag {coolr_dic} is required !")
                continue

            if 'sqlite' in coolr_dic['schemaName']:
                logger.error(f"Tag {tag} is in sqlite - {coolr_dic}")
            else:
                # Get the list of COOL intervals, using the run range if provided
                cool_intervals = []
                if args.run:
                    # shift the run range by 3600 seconds (10 minutes) to account for possible delays
                    s = int((runstart_timestamp-24*one_hour)*1000000000)
                    e = int((runend_timestamp)*1000000000)
                    if 'run' in tag_type:
                        s = run << 32
                        e = (run+1) << 32
                        logger.info(f"Tag {tag} is run based, using run range: {s} - {e}")
                        cool_intervals = load_intervals(oracle_connection, coolr_dic['schemaName'], coolr_dic['dbname'], coolr_dic['node'], coolr_dic['tag_name'], 0, 0, cool_max_validity)
                        cool_intervals_filtered = filter_by_range(cool_intervals, s, e)
                        logger.info(f"Tag {tag} has {len(cool_intervals)} intervals in COOL for the run range, filtered: {len(cool_intervals_filtered)}")
                        cool_intervals = cool_intervals_filtered
                    else:
                        logger.info(f"Tag {tag} is time based, using run range: {s} - {e}")
                        cool_intervals = load_intervals(oracle_connection, coolr_dic['schemaName'], coolr_dic['dbname'], coolr_dic['node'], coolr_dic['tag_name'], 0, s, e)
                        if (len(cool_intervals) == 0):
                            cool_intervals = load_intervals(oracle_connection, coolr_dic['schemaName'], coolr_dic['dbname'], coolr_dic['node'], coolr_dic['tag_name'], 0, 0, cool_max_validity)
                            cool_intervals_filtered = filter_by_range(cool_intervals, s, e)
                            logger.info(f"Tag {tag} has {len(cool_intervals)} intervals in COOL for the run range, filtered: {len(cool_intervals_filtered)}")
                            cool_intervals = cool_intervals_filtered

                    crest_iovs = get_crest_iovs(name=tag,since=s,until=e)
                    if crest_iovs['size'] > 0:
                        last_crest_iov = int(crest_iovs['resources'][0]['since'])
                    else:
                        logger.info(f"Tag {tag} has no IOVs in CREST for the run range, keep using the last one {last_crest_iov}")
                else:
                    cool_intervals = load_intervals(oracle_connection, coolr_dic['schemaName'], coolr_dic['dbname'], coolr_dic['node'], coolr_dic['tag_name'], 0, 0, cool_max_validity)
                # Compare with the last IOV in CREST
                if last_crest_iov is not None:
                    last_cool_iov = cool_intervals[-1][0]
                    logger.info(f"Tag {tag} : compare last IOV in COOL {last_cool_iov} with last IOV in CREST {last_crest_iov}")
                    if last_cool_iov == last_crest_iov:
                        logger.info(f"Tag {tag} has the same last IOV in CREST and COOL - {coolr_dic}")
                    elif last_cool_iov < last_crest_iov:
                        logger.info(f"Tag {tag} has last IOV in COOL < last IOV in CREST [IGNORE DIFFERENCE]: cool - {last_cool_iov} crest - {last_crest_iov} - {coolr_dic}")
                    else:
                        logger.warning(f"Tag {tag} has different last IOV in CREST and COOL: cool - {last_cool_iov} crest - {last_crest_iov} - {coolr_dic}")
                        # Here we should dump a command for the migration of the corresponding cool tag
                        logger.info(f'CoolDump --db \"oracle://ATONR_ADG;schema=ATLAS_{coolr_dic["schemaName"]};dbname={coolr_dic["dbname"]};user=ATLAS_COOL_READER;password=$CRPASS\" --tag {cool_tag} --node \"{coolr_dic["node"]}\" --since \"{last_crest_iov}\" --until \"{cool_max_validity}\"')
                        host_write = 'http://atlaf-alma9-01.cern.ch:8080/api-v4.0'
                        logger.info(f'AtlCool2CREST {coolr_dic["schemaName"]}/{coolr_dic["dbname"]} {host_write} -crest -folder \"{coolr_dic["node"]}\" -tag \"{cool_tag}\" -globaltag {globaltag}')
                else:
                    logger.error(f"Tag {tag} has no IOVs in CREST - {coolr_dic}")
    if oracle_connection:
            oracle_connection.close()

if __name__ == "__main__":
    main()
