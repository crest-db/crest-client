from PyCool import cool
import json
import time

## Please REMOVE the file if it is there
locconnstr = "sqlite://;schema=simplecool.db;dbname=CONDBR2"
dbSvc=cool.DatabaseSvcFactory.databaseService()
locdbconn=None

# In the following we list functions to interact with the COOL database
## Create a folder specification: this is a dictionary of column names and types
def createSpec(coldic={}):
    rspec = cool.RecordSpecification()
    for k,v in coldic.items():
        rspec.extend(k,v)
    return cool.FolderSpecification(cool.FolderVersioning.MULTI_VERSION, rspec)

## Create a folder: needs a database connection, a folder name and a folder specification
def createFolder(db, folder, desc, spec):
    loc_folder = db.createFolder(folder, spec, desc, True)
    return loc_folder

## Get a folder
def getFolder(db, folder):
    return db.getFolder(folder)

## Get objects from a folder and a time range and tag name
def getObjects(cool_folder, tag, since, until):
    objs = cool_folder.browseObjects(since, until, cool.ChannelSelection.all(), tag)
    return objs

## Add a row to a folder
def addRow(fld=None, record={}):
    rspec = fld.payloadSpecification()
    r = cool.Record(rspec)
    for k,v in record.items():
       r[k] = v
    return r


def random_data():
    import random
    import string
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=2048))

def random_record():
    import random
    return {
        'filedata': random_data(),
        'file': random_data(),
        'tech': random.randint(0, 100),
    }

# Create a dictionary for folder specification
coldic = { 'filedata': cool.StorageType.String4k, 'file': cool.StorageType.String4k, 'tech': cool.StorageType.Int32 }
destfolder='/TEST/CREST'
desttag='TestCrest-REL-01'
folderdesc =  '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>'

# Create a payload record
# use the columns defined before to create a typical payload: we use a Cool compatible format

# Start creating everything in Cool
try:
    # Connect to Cool DB (in this case create an sqlite file)
    locdbconn = dbSvc.createDatabase(locconnstr)
    print(f'Opened database {locconnstr}')

    start_time = time.time()
    # Create record specifications
    spec = createSpec(coldic)
    print(f'Created specification {spec}')

    # Create a folder
    cool_folder = createFolder(locdbconn, destfolder, folderdesc, spec)
    print(f'Created folder {cool_folder}')

    print('Storing new bulk data')
    cool_folder.setupStorageBuffer()
    # Add iov
    # Insert data into the folder and tag
    for since in range(0, 10000, 100):
        mmrec = random_record()
        rec = addRow(cool_folder, mmrec)
        cool_folder.storeObject(since,  cool.ValidityKeyMax, rec, 1, desttag)
        mmrec = random_record()
        rec = addRow(cool_folder, mmrec)
        cool_folder.storeObject(since,  cool.ValidityKeyMax, rec, 2, desttag)

    cool_folder.flushStorageBuffer()
    end_time = time.time()
    print(f'Time elapsed for iovs insertion : {end_time - start_time} seconds')
except Exception as e:
    print("Exception when calling Cool for folder creation and iov storage: %s\n" % e)

# Verify your data
# Query data from the folder and tag
print('List iovs and payloads for tag %s' % desttag)
try:
    start_time = time.time()
    cool_data = getObjects(cool_folder, desttag, 1000, 4000)
    print(f'Retrieved data: {cool_data}')
    end_time = time.time()
    print(f'Time elapsed for iovs and payloads retrieval : {end_time - start_time} seconds')
except Exception as e:
    print("Exception when calling Cool for iov and payload access: %s\n" % e)

# Close the sqlite
locdbconn.closeDatabase()
# Remove the sqlite before running again the script