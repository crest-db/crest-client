from pycrest.api.crest_api import CrestApi
from pycrest.api.crest_cond_container import CrestCondContainer
from pycrest.api.tag_info_container import TagInfoContainer
from pycrest.api.crest_cond_builder import CrestCondBuilder
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.tag_meta_dto import TagMetaDto
import json
import sys
import time

# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-undertow-api.web.cern.ch/api-v4.0')
api_instance = CrestApi(host=host)
socks = os.getenv('CREST_SOCKS', 'False')
if socks == 'True':
    api_instance.socks()

def random_data():
    import random
    import string
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=2048))

def random_record():
    import random
    return {
        'filedata': random_data(),
        'file': random_data(),
        'tech': random.randint(0, 100),
    }

# Define tag
desttag='TestCrest-REL-01'

# Clean up the existing tag
# Remove the tag
# Clean up the existing tag
# Remove the tag
print('Delete tag %s if exists' % desttag)
try:
    tag = api_instance.find_tag(name=desttag)
    if tag is not None and isinstance(tag, TagDto):
        tagname = tag.name
        description = tag.description
        print(f'Found tag {tagname} - {description} for name {desttag}: removing it...')
        api_response = api_instance.remove_tag(name=desttag)
except Exception as e:
    print("Exception when calling CrestApi for tag removal: %s\n" % e)

# tag description
description = 'A CREST tag'
# tag time type
time_type = 'time'
# Tag meta information
destfolder='/TEST/CREST'
node_description='<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\"/></addrHeader><typeName>CondAttrListCollection</typeName>'
coldic = { 'filedata': 'String4k', 'file': 'String4k', 'tech': 'Int32' }
chandic = { '1': 'CHAN-1', '2': 'CHAN-2', '3': 'CHAN-3', '4': 'CHAN-4', '5': 'CHAN-5', '6': 'CHAN-6', '7': 'CHAN-7', '8': 'CHAN-8'}

builder = CrestCondBuilder(schema='COOLOFL_TEST', folder=destfolder, tagname=desttag)
builder.init_container(coldic, node_description, chandic)

# Create a payload record
# use the columns defined before to create a typical payload: we use a Cool compatible format


# Start creating everything in Crest
try:
    start_time = time.time()

    # Create a new tag
    print(f'Creating tag {desttag} with time type {time_type} and description {description}')
    tag_dto = builder.build_tag(tag_description=description, time_type=time_type)

    api_response = api_instance.create_tag(tag_dto)
    if isinstance(api_response, TagDto):
        print(f'Created tag {api_response.name}')
    else:
        print(f'Error creating tag {desttag}: {api_response}')
    # Add tag info
    tag_info_dto = builder.get_tag_info().get_tag_info_dto()
    print(f'Creating tag meta info for {desttag} with {tag_info_dto}')
    api_response = api_instance.create_tag_meta(tag_info_dto)
    if isinstance(api_response, TagMetaDto):
        print(f'Created tag meta info for {api_response.tag_name}')
    else:
        print(f'Error creating tag meta info for {desttag}: {api_response}')
        sys.exit(1)
    # Start creating a payload that will be stored in Crest
    print(f'Create a payload as a condition container')
    crest_payload = builder.build_cond_container()
    payload_streamer = {'author': 'myname'}
    for since in range(0, 10000, 100):
        rec = random_record()
        crest_payload.add_record(attribute_list=rec)
        crest_payload.add_data(channel_id=1, record=rec)
        rec = random_record()
        crest_payload.add_record(attribute_list=rec)
        crest_payload.add_data(channel_id=2, record=rec)
        crest_payload.add_iov(since=since, data=crest_payload.get_payload(), streamer_info=payload_streamer)

    # Insert data into the tag
    store_set_dto = crest_payload.get_store_set()
    api_response = api_instance.store_data(tag=desttag, store_set=store_set_dto)
    print(f'Stored data in tag {desttag}')
    end_time = time.time()
    print(f'Elapsed time: {end_time - start_time} seconds')

except Exception as e:
    print("Exception when calling CrestApi : %s\n" % e)

# Verify your data using crest-read.py