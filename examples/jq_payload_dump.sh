#!/bin/bash

# URL template where you send the payloadHash
BASE_URL="https://crest.cern.ch/api-v4.0/payloads"

# Iterate over each payloadHash and send the curl request
jq -r '.resources[].payloadHash' response.json | while read -r payloadHash; do
    echo "Downloading file for payloadHash: $payloadHash"

    # Send the curl request, saving the output to a file named by the payloadHash
    curl -o "${payloadHash}.bin" "${BASE_URL}/${payloadHash}"

    echo "Downloaded: ${payloadHash}.bin"
done
