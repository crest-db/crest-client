from pycrest.api.crest_api import CrestApi
from hep.crest.client.model.iov_set_dto import IovSetDto

# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-undertow-api.web.cern.ch/api-v4.0')
api_instance = CrestApi(host=host)
socks = os.getenv('CREST_SOCKS', 'False')
if socks == 'True':
    api_instance.socks()

# global tag
global_tag = 'CREST-MC23-SDR-RUN3-02'
try:
    # Find the global tag associated tags
    api_response = api_instance.find_global_tag_map(name=global_tag)
    print(f'Retrieved global tag map : {api_response}')
    global_tag_map_set = api_response
    resources = global_tag_map_set.resources
    for r in resources:
        print(f'  {r}')
        iov_set_dto = api_instance.find_all_iovs(r['tagName'])
        iovs = iov_set_dto.resources
        niovs = 0
        for iov in iovs:
            niovs += 1
            print(f'    {iov}')
            iov_data = api_instance.get_payload(iov.payload_hash)
            if niovs > 20:
                break
except Exception as e:
    print("Exception in reading data: %s\n" % e)
