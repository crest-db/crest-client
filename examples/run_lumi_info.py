from pycrest.api.crest_api import CrestApi
from datetime import datetime

# create an instance of the API class
api_instance = CrestApi(host='http://crest-undertow-api.web.cern.ch/api-v4.0')
#api_instance = CrestApi(host='http://localhost:8090/api')
run = 150
lumi = 200
st = datetime(2020, 2, 10, 0, 0, 0)
et = datetime(2020, 2, 11, 0, 0, 0)

print(f'Create a run-lumi entry for run {run} lumi {lumi}')
try:
    # Create a new run lumi entry
    api_response = api_instance.create_run_lumi(run=run, lumiblock=lumi, startdate=st.timestamp(), enddate=et.timestamp())
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->create_run_lumi: %s\n" % e)

since = 10 
until = 150
print(f'Get data for run range since {since} until {until}')
runlbset = api_instance.list_run_lumi(since=since, until=until, mode='runrange')
print(f'Select set {runlbset} for since {since}')
