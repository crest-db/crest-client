from pycrest.api.crest_api import CrestApi
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.store_set_dto import StoreSetDto
from hep.crest.client.model.global_tag_dto import GlobalTagDto
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.store_dto import StoreDto


# create an instance of the API class
def socks(proxy_host):
    SOCKS5_PROXY_HOST = proxy_host
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

socks('localhost')
api_instance = CrestApi(host='http://crest-j23.cern.ch:8080/api-v5.0')

# global tag
global_tag = 'test_global_tag'
description = 'a brand new test global tag'
print('Create a global tag %s' % global_tag)
try:
    globaltag = GlobalTagDto(
        name=global_tag,
        description=description,
        release='r1',
        scenario='test',
        validity=0,
        workflow='test',
        type='T'
    )

    # Create a new global tag
    api_response = api_instance.create_global_tag(dto=globaltag, force='false')
    print(api_response)
    # Find the global tag
    api_response = api_instance.find_global_tag(name=global_tag)
    print(f'Retrieved global tag : {api_response}')
except Exception as e:
    print("Exception when calling CrestApi->create_global_tag: %s\n" % e)

# tag name
name = 'test_tag'
# tag description
description = 'a brand new test tag'
# tag time type
time_type = 'time'

print('Create a tag %s' % name)
try:
    # Create a TagDto object
    tagdto = TagDto(
        name=name,
        description=description,
        time_type=time_type,
        payload_spec='crest-json-single-iov',
        synchronization='none',
        last_validated_time=0,
        end_of_validity=0

    )
    # Create a new tag
    api_response = api_instance.create_tag(tagdto)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->create_tag: %s\n" % e)

# Insert data into the tag
resources = [
    {'since': 1000, 'data': 'test data 1, it should be some json...', 'streamerInfo': 'test streamer info 1'},
    {'since': 2000, 'data': 'test data 2, remember to base64 encode binary...', 'streamerInfo': 'test streamer info 2'},
    {'since': 3000, 'data': 'test data 3, another json', 'streamerInfo': 'test streamer info 3'},
]
store_set=StoreSetDto(
    size=len(resources),
    datatype="iovs",
    format="StoreSetDto",
    resources=[]
)
# Create a StoreDto object for each resource
for resource in resources:
    storedto = StoreDto(
        since=resource['since'],
        data=resource['data'],
        streamerInfo=resource['streamerInfo']
    )
    store_set.resources.append(storedto)
print('Store data in tag %s' % name)
try:
    api_response = api_instance.store_data(tag=name, store_set=store_set)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->store_json_data: %s\n" % e)

# Query data from the tag
print('List iovs for tag %s' % name)
iovs = []
try:
    api_response = api_instance.find_all_iovs(tagname=name)
    print(api_response)
    if isinstance(api_response, IovSetDto):
        iovs = api_response.resources
        for iov in iovs:
            print(f'Found iov : {iov}')
except Exception as e:
    print("Exception when calling CrestApi->find_all_iovs: %s\n" % e)

# Get data for a given since

since = 2020
print(f'Get data for since {since}')
sel_hash = api_instance.find_payload_hash(iovs, since)
print(f'Select hash {sel_hash} for since {since}')
try:
    api_response = api_instance.get_payload(hash=sel_hash)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->get_payload: %s\n" % e)

# Mappings
record='test_record'
label='test_label'

try:
    print('Create mapping')
    api_response = api_instance.create_global_tag_map(globaltagname=global_tag, tagname=name, record=record, label=label)
    print(api_response)
    api_response = api_instance.find_global_tag_map(name=global_tag)

except Exception as e:
    print("Exception when calling CrestApi->create_mapping: %s\n" % e)

# Remove the mapping
try:
    print('Remove mapping')
    api_response = api_instance.delete_global_tag_map(globaltagname=global_tag, tagname=name, label=label)
    print('Done')
except Exception as e: 
    print("Exception when calling CrestApi->delete_global_tag_map: %s\n" % e)

# Remove the tag
print('Delete tag %s' % name)
try:
    api_response = api_instance.remove_tag(name=name)
    print('Done')
except Exception as e:
    print("Exception when calling CrestApi->remove_tag: %s\n" % e)

# Remove the global tag
print('Delete global tag %s' % global_tag)
try:
    api_response = api_instance.remove_global_tag(name=global_tag)
    print('Done')
except Exception as e:
    print("Exception when calling CrestApi->remove_global_tag: %s\n" % e)
