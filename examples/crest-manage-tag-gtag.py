from pycrest.api.crest_api import CrestApi
from pycrest.api.tag_info_container import TagInfoContainer
from pycrest.api.crest_cond_builder import CrestCondBuilder
from pycrest.api.crest_cond_tag_manager import TagManager
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.tag_meta_dto import TagMetaDto
from hep.crest.client.model.global_tag_dto import GlobalTagDto
from hep.crest.client.model.global_tag_map_dto import GlobalTagMapDto
from hep.crest.client.model.global_tag_map_set_dto import GlobalTagMapSetDto
from hep.crest.client.model.tag_meta_set_dto import TagMetaSetDto
import json
# Author: Andrea Formica (CERN)
# Example for Global Tag Coordination.
#
# In this snippet we will create some tags with the tag info as a system expert would do.
# Then we will create a global tag and associate the tags to it.
# We are going to implement the LOCK and UNLOCK features in the CREST server later on.

# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-undertow-api.web.cern.ch/api-v4.0')
api_instance = CrestApi(host=host)
socks = os.getenv('CREST_SOCKS', 'False')
if socks == 'True':
    api_instance.socks()

# Define tags and metadata
meta_info = {
    'Test-LARBadChannelsOflBadChannels-RUN3-00': {
        'description': 'A Test LAR CREST tag',
        'time_type': 'time',
        'payload_spec': {'AnInt': 'UInt32', 'ABlob': 'Blob64k'},
        'node_description': '<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\"/></addrHeader><typeName>CondAttrListCollection</typeName>',
        'folder': '/LAR/BadChannelsOfl/BadChannels',
        'schema': 'COOLOFL_LAR'
    },
    'Test-TileOfl02CalibCes-RUN3-00': {
        'description': 'A Test Tile CREST tag',
        'time_type': 'time',
        'payload_spec': {'TileCalibBlob' : 'Blob64k'},
        'node_description': '<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\"/></addrHeader><typeName>CondAttrListCollection</typeName>',
        'folder': '/TILE/OFL02/CALIB/CES',
        'schema': 'COOLOFL_TILE'
    },
    'Test-MuonAlignMDTBarrel-RUN3-00': {
        'description': 'A Test Muon CREST tag',
        'payload_spec': {'filedata': 'String4k', 'file': 'String4k', 'tech':'Int32'},
        'node_description': '<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\"/></addrHeader><typeName>CondAttrListCollection</typeName>',
        'time_type': 'time',
        'folder': '/MUONALIGN/MDT/BARREL',
        'schema': 'COOLOFL_MUON'
    }
}

def delete_tag(tag_name):
    # Clean up the existing tag
    # Remove the tag
    print('Delete tag %s if exists' % tag_name)
    try:
        tag = api_instance.get_tag(name=tag_name)
        if tag is not None and isinstance(tag, TagDto):
            tagname = tag.name
            print(f'Found tag {tag} : removing it...')
            api_response = api_instance.remove_tag(name=tagname)
            print(f'Removed tag {tagname}')
    except Exception as e:
        print("Exception when calling CrestApi: %s\n" % e)

def create_crest_tag(tag_name, description, time_type, builder):
    try:
        # Create a new tag
        print(f'Process creation for tag {tag_name}')
        tag = api_instance.get_tag(name=tag_name)
        if tag is not None and isinstance(tag, TagDto):
            print(f'Found tag {tag.name}... do not recreate it')
        else:
            api_response = api_instance.create_tag(name=tag_name, timeType=time_type, description=description)
            if isinstance(api_response, TagDto):
                print(f'Created tag {api_response.name}')
            else:
                print(f'Error creating tag {tag_name}: {api_response}')
                return None
            # Add tag info
            params = builder.get_tag_info().get_tag_info_params()
            print(f'Creating tag meta info for {tag_name}')
            api_response = api_instance.create_tag_meta(name=tag_name, **params)
            if isinstance(api_response, TagMetaDto):
                print(f'Created tag meta info for {api_response.tag_name}')
            else:
                print(f'Error creating tag meta info for {tag_name}: {api_response}')
    except Exception as e:
        print("Exception when calling CrestApi : %s\n" % e)


# Create a global tag
global_tag_name = 'Test-GlobalTag-RUN3-00'

# Delete the global tag and the mappings
print(f'Delete global tag {global_tag_name} if exists')
try:
    gtag = api_instance.get_global_tag(name=global_tag_name)
    if gtag is not None and isinstance(gtag, GlobalTagDto):
        print(f'Found global tag {gtag.name}, remove it together with all mappings...')
        api_response = api_instance.find_global_tag_map(name=gtag.name)
        mappings = api_response.resources
        for mapping in mappings:
            print(f'Found mapping {mapping}')
            if isinstance(mapping, GlobalTagMapDto):
                print(f'Remove mapping {mapping}')
                api_response = api_instance.delete_global_tag_map(globaltagname=gtag.name, tagname=mapping.tag_name, label=mapping.label)
                delete_tag(mapping.tag_name)
        print(f'Remove global tag {gtag.name}')
        api_response = api_instance.remove_global_tag(name=gtag.name)
except Exception as e:
    print("Exception when calling CrestApi: %s\n" % e)

# Create the tags
chandic = { '1': 'CHAN-1', '2': 'CHAN-2', '3': 'CHAN-3', '4': 'CHAN-4', '5': 'CHAN-5', '6': 'CHAN-6', '7': 'CHAN-7', '8': 'CHAN-8'}
for tag, info in meta_info.items():
    print(f'Create tag {tag}')
    bld = CrestCondBuilder(schema=info['schema'], folder=info['folder'], tagname=tag)
    bld.init_container(p_spec=info['payload_spec'], n_description=info['node_description'], c_dict=chandic)
    create_crest_tag(tag, info['description'], info['time_type'], bld)

print(f'Create global tag {global_tag_name}')
try:
    gtdto = None
    api_response = api_instance.create_global_tag(name=global_tag_name, description='A Test Global Tag')
    print(api_response)
    if isinstance(api_response, GlobalTagDto):
        print(f'Created global tag {api_response.name}')
        gtdto = api_response
    else:
        print(f'Error creating global tag {global_tag_name}: {api_response}')
    # Associate the tags to the global tag
    mapping = TagManager(global_tag=gtdto)
    for tag, info in meta_info.items():
        print(f'Prepare mapping for tag {tag} to global tag {global_tag_name}')
        record = 'test_record'
        api_response = api_instance.find_tag_meta(name=tag)
        if isinstance(api_response, TagMetaSetDto):
            tagmeta = api_response.resources[0]
            print(f'Found tag meta {tagmeta}')
            mapping.add_tag(tag_meta=tagmeta, record=record)
        else:
            print(f'Error preparing mapping for tag {tag} to global tag {global_tag_name}')
    # Loop over the mappings which we have prepared in memory and store them in CREST
    for map in mapping.get_mappings():
        for tag, p in map.items():
            print(f'Create mapping for {tag} and {p}')
            api_response = api_instance.create_global_tag_map(globaltagname=global_tag_name, tagname=tag, **p)
            print(api_response)

    api_response = api_instance.find_global_tag_map(name=global_tag_name)
    print(f'List global tag mappings {api_response}')
    if isinstance(api_response, GlobalTagMapSetDto):
        tracemap = (api_response)
        print(f'Found mappings {tracemap}')
except Exception as e:
    print("Exception when calling CrestApi to create global tag and mappings: %s\n" % e)
