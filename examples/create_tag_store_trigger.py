from pycrest.api.crest_api import CrestApi
from hep.crest.client.models import (
    IovSetDto, HTTPResponse, TagMetaSetDto, TagMetaDto, TagSetDto, TagDto, GlobalTagDto, 
    GlobalTagSetDto, GlobalTagMapDto, StoreSetDto, StoreDto, RunLumiInfoDto, RunLumiSetDto, IovDto, IovSetDto
)
# create an instance of the API class
def socks(proxy_host):
    SOCKS5_PROXY_HOST = proxy_host
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

##socks('localhost')
api_instance = CrestApi(host='http://localhost:8082/api')

# tag name
name = 'TRIGGER_DB_L1PSK'
# tag description
description = 'a brand new test tag for trigger DB'
# tag time type
time_type = 'run-lumi'

params = {
    'payload_spec': 'json',
    'description': description,
    'synchronization': 'none',
    'last_validated_time': -1.,
    'end_of_validity': -1.,
}
tagdto = TagDto(
    name=name,
    description=params['description'],
    time_type=time_type,
    payload_spec=params['payload_spec'],
    synchronization=params['synchronization'],
    last_validated_time=params['last_validated_time'],
    end_of_validity=params['end_of_validity']
)

p_hash = 'triggerdb://ATLAS_CONF_TRIGGER_RUN3/L1PSK/13447'
dto = IovDto(since=int(1234), tag_name=name, payload_hash=p_hash)
iovset = IovSetDto(resources=[dto], size=1, format='IovSetDto', datatype='iovs')

print('Create a tag %s' % name)
try:
    # activate socks
    # Create a new tag
    api_response = api_instance.create_tag(tagdto)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->create_tag: %s\n" % e)

print('Find a tag %s' % name)
try:
    # activate socks
    # Create a new tag
    api_response = api_instance.find_tag(name=name)
    print(api_response)
    api_response = api_instance.store_iovs(iov_set_dto=iovset)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->find_tag or store_iovs: %s\n" % e)

print('Find payload %s' % p_hash)
try:
    # activate socks
    # Create a new tag
    api_response = api_instance.get_payload(hash=p_hash)
    print(api_response)
except Exception as e:
    print("Exception when calling CrestApi->get_payload: %s\n" % e)


# Remove the tag
print('Delete tag %s' % name)
try:
    api_response = api_instance.remove_tag(name=name)
    print('Done')
except Exception as e:
    print("Exception when calling CrestApi->remove_tag: %s\n" % e)
