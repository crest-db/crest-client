from pycrest.api.crest_api import CrestApi
from pycrest.api.crest_cond_container import CrestCondContainer
from pycrest.api.crest_cond_builder import CrestCondBuilder
from pycrest.api.tag_info_container import TagInfoContainer
from hep.crest.client.model.iov_set_dto import IovSetDto
from hep.crest.client.model.tag_dto import TagDto
from hep.crest.client.model.tag_meta_dto import TagMetaDto
import json
import secrets
import time

# Author: Andrea Formica (CERN)
# Example taken from TILE COOL data, using folder /TILE/OFL02/CALIB/CES.
#
# In this snippet we will create a new tag, add tag info and add iovs to the tag.
# The tag will be created with a specific payload specification and a set of channels
# The iovs will be created with a random payload for each channel, and the since will be incremented by 100.
# The data are stored in one go at the end of the process, for all the iovs.
# This avoid the need to call crest api http requests for each iov.
#
# To read the data back use the script crest-read-tile.py
# Every time you run this script you will create a new tag with new data, and remove the old one.


# create an instance of the API class
import os
host = os.getenv('CREST_HOST', 'http://crest-undertow-api.web.cern.ch/api-v4.0')
api_instance = CrestApi(host=host)
socks = os.getenv('CREST_SOCKS', 'False')
if socks == 'True':
    api_instance.socks()

# Define tag
desttag='Test-TileOfl02CalibCes-RUN2-01'

# tag description
description = 'A Test Tile CREST tag'
# tag time type
time_type = 'time'
# Create channel dictionary
channel_dict = {}
for chan in range(0, 50):
    entry = { str(chan) : f'TileCalib-0{chan}'}
    channel_dict.update(entry)

# Created channel dictionary
print(f'Created channel dictionary: {channel_dict} of size {len(channel_dict)}')

# Define a class to create the tag meta information
class TileCalibCes(CrestCondBuilder):
    p_spec = { 'TileCalibBlob': 'Blob64k' }
    c_dict = channel_dict
    n_description = '<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\"/></addrHeader><typeName>CondAttrListCollection</typeName>'
    schema = 'COOLOFL_TILE'
    folder = '/TILE/OFL02/CALIB/CES'
    tagname = desttag

    def __init__(self):
        """
        Constructor
        Use the schema, folder and tagname to create a new tag meta info.
        Use the properties then to finalize the tag meta creation.
        """
        CrestCondBuilder.__init__(self, schema=self.schema, folder=self.folder, tagname=self.tagname)
        CrestCondBuilder.init_container(self, p_spec=self.p_spec, n_description=self.n_description, c_dict=self.c_dict)


tilecond = TileCalibCes()

def generate_random_binary_payload(num_bytes):
    # Generate a random binary sequence with the specified number of bytes
    random_bytes = secrets.token_bytes(num_bytes)
    return random_bytes

# Create a payload record
# use the columns defined before to create a typical payload: we use a Cool compatible format
# In this function we emulate new data for different run-lumi and channels
def generate_since_data(since=0, crest_payload=None):
    payload_streamer = {'author': 'tileexpert', 'comment': 'this has new Blob data'}
    for chan in range(0, 50):
        lob = generate_random_binary_payload(10000)
        rec = crest_payload.add_record(attribute_list={'TileCalibBlob': lob})
        crest_payload.add_data(channel_id=chan, record=rec)
    crest_payload.add_iov(since=since, data=crest_payload.get_payload(), streamer_info=payload_streamer)
    return crest_payload
    

# Clean up the existing tag
# Remove the tag
print('Delete tag %s if exists' % desttag)
try:
    tag = api_instance.find_tag(name=desttag)
    if tag is not None and isinstance(tag, TagDto):
        tagname = tag.name
        description = tag.description
        print(f'Found tag {tagname} - {description} for name {desttag}: removing it...')
        api_response = api_instance.remove_tag(name=desttag)
except Exception as e:
    print("Exception when calling CrestApi: %s\n" % e)

# Start creating everything in Crest
try:
    start_time = time.time()
    # Create a new tag
    tag_dto = tilecond.build_tag(tag_description=description, time_type=time_type)
    api_response = api_instance.create_tag(tag_dto)
    if isinstance(api_response, TagDto):
        print(f'Created tag {api_response.name}')
    else:
        print(f'Error creating tag {desttag}: {api_response}')
    # Add tag info
    tag_info_dto = tilecond.get_tag_info().get_tag_info_dto()
    print(f'Creating tag meta info for {desttag} with params {tag_info_dto}')

    api_response = api_instance.create_tag_meta(tag_info_dto)
    if isinstance(api_response, TagMetaDto):
        print(f'Created tag meta info for {api_response.tag_name}')
    else:
        print(f'Error creating tag meta info for {desttag}: {api_response}')
    # Add iovs
    crest_payload = tilecond.get_payload()
    for since in range(0, 10000, 1000):
        crest_payload = generate_since_data(since=since, crest_payload=crest_payload)
        print(f'Add data for since {since}')
    store_set_dto = crest_payload.get_store_set()
    api_response = api_instance.store_data(tag=desttag, store_set=store_set_dto)
    print('Stored data in tag %s' % desttag)
    end_time = time.time()
    # Calculate the elapsed time
    elapsed_time = end_time - start_time
    print("Time elapsed:", elapsed_time, "seconds")

except Exception as e:
    print("Exception when calling CrestApi : %s\n" % e)

